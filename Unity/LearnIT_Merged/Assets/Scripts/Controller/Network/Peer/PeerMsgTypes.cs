﻿using UnityEngine;
using UnityEngine.Networking;


namespace PeerMsgTypes
{
	public enum PeerEvent
	{
		ResponseLogReceived
	}

	// -------------- client to server messages --------------

	public class MatchInvitationRequest : MessageBase
	{
        public const short ID = 150;

        public int requestedGameModeID;
        public string requestingPlayerName;
	}

    public class CancelMatchInvitationRequest : MessageBase
    {
        public const short ID = 151;

        public string requestingPlayerName;
    }

    public class ReadyToJoinLobbyMsg : MessageBase
    {
        public const short ID = 152;
    }

	// -------------- server to client messages --------------

	public class MatchInvitationResponse : MessageBase
	{
        public const short ID = 160;

		public bool accepted;

        public MatchInvitationResponse() { }

	}

    public class CancelMatchInvitationResponse : MessageBase
    {
        public const short ID = 161;
    }

}
