﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using Utils;

public class NetworkDiscoveryService : MonoBehaviour
{
    List<INetworkChangedHandler> onNetworkChangedHandlers = new List<INetworkChangedHandler>();

    //broadcast protocoll
    private class DiscoveryMessage
    {
        //example broadcast message / protocol: APP_NAME;DEVICE_ID;PLAYER_NAME;IS_PLAYING;
        private static readonly string[] MESSAGE_SEPARATOR = new string[] { ";;;" };
        private static string VALIDATION_MESSAGE = new DiscoveryMessage(Global.APPLICATION_NAME, "0", "0", false).ToString();

        public static DiscoveryMessage StringToMessage(string msg)
        {
            //validate message
            if (!Validate(msg))
                return null;

            //read message
            string[] msgArray = msg.Split(MESSAGE_SEPARATOR, StringSplitOptions.None);
            string appName = msgArray[0];
            string clientID = msgArray[1];
            string clientName = msgArray[2];
            bool isPlaying = !msgArray[3].Equals("0");
            return new DiscoveryMessage(appName, clientID, clientName, isPlaying);
        }

        public static bool Validate(string message)
        {
            if (null == message)
            {
                Debug.LogError("NetworkDiscovery: Validate Message: message is null");
                return false;
            }

            if (!message.Contains(MESSAGE_SEPARATOR[0]))
            {
                Debug.LogError("NetworkDiscovery: Validate Message: message has no separator");
                return false;
            }

            string[] msgArray = message.Split(MESSAGE_SEPARATOR, StringSplitOptions.None);
            string[] validationArray = VALIDATION_MESSAGE.Split(MESSAGE_SEPARATOR, StringSplitOptions.None);

            //check number of protocol segments
            if (msgArray.Length != validationArray.Length)
            {
                Debug.LogError("NetworkDiscovery: Validate Message: unexpected protocol size (number of parts)");
                return false;
            }

            //check appName
            if (!msgArray[0].Equals(validationArray[0]))
            {
                Debug.LogError("NetworkDiscovery: Validate Message: unexpected application name");
                return false;
            }

            return true;
        }

        public static DiscoveryMessage GenerateByClientInfo(LocalClientInfo localClientInfo)
        {
            string appName = Global.APPLICATION_NAME;
            string clientID = localClientInfo.GetID();
            string clientName = localClientInfo.GetPlayerName();
            bool isPlaying = localClientInfo.IsPlaying();

            return new DiscoveryMessage(appName, clientID, clientName, isPlaying);
        }

        public string appName;
        public string clientID;
        public string clientName;
        public bool isPlaying;

        public DiscoveryMessage(string appName, string clientID, string clientName, bool isPlaying){
            this.appName = appName;
            this.clientID = clientID;
            this.clientName = clientName;
            this.isPlaying = isPlaying;
        }

        public override string ToString()
        {
            //join the strings together and separate them by the separator. 
            string[] msgArray = new string[] { appName, clientID, clientName, isPlaying ? "1" : "0" };
            return string.Join(MESSAGE_SEPARATOR[0], msgArray);
        }
    }

    
    private const float CLIENT_TIME_OUT = 5; //seconds without update before client will be removed from list

    private string messageToBroadcast;
    private NetworkBroadcastReceiver broadcastReceiver;
    private NetworkBroadcastSender broadcastSender;
    private List<NetworkClientInfo> discoveredClients = new List<NetworkClientInfo>();
    private LocalClientInfo localClient;

    public bool IsBroadcasting()
    {
        return broadcastSender.IsRunning();
    }

    public bool IsListening()
    {
        return broadcastReceiver.IsRunning();
    }

    public ReadOnlyCollection<NetworkClientInfo> GetDiscoveredClients()
    {
        return discoveredClients.AsReadOnly();
    }

    public void RegisterNetworkChangedHandler(INetworkChangedHandler handler)
    {
        if (!onNetworkChangedHandlers.Contains(handler))
            onNetworkChangedHandlers.Add(handler);
    }

    public void RemoveNetworkChangedHandler(INetworkChangedHandler handler)
    {
        if (onNetworkChangedHandlers.Contains(handler))
            onNetworkChangedHandlers.Remove(handler);
    }

    public void StartBroadcasting()
    {
        if (null == messageToBroadcast)
        {
            Debug.LogError("NetworkDiscovery: Can not start broadcasting. No Information to Broadcast was specified.");
            return;
        }
        
        broadcastSender.SetBroadcastData(messageToBroadcast);
        broadcastSender.StartService();
    }

    public void StopBroadcasting()
    {
        broadcastSender.StopService();
    }

    public void StartListening()
    {
        broadcastReceiver.StartService();
    }

    public void StopListening()
    {
        broadcastReceiver.StopService();
    }

    void Awake()
    {
        //init broadcast sender, put the message to broadcast and start sending
        broadcastSender = gameObject.AddComponent<NetworkBroadcastSender>();
        broadcastSender.Initialize();

        //init broadcast receiver and start listening
        broadcastReceiver = gameObject.AddComponent<NetworkBroadcastReceiver>();
        broadcastReceiver.AddOnBroadcastReceiveAction(HandleBroadcastMessage);
        broadcastReceiver.Initialize();
    }

    void Update()
    {
        //remove timed-out clients
        List<NetworkClientInfo> clientsToRemove = null;
        foreach (NetworkClientInfo clientInfo in discoveredClients)
        {
            float timeSinceLatestUpdate = Time.time - clientInfo.GetLatestUpdateReceived();
            if (timeSinceLatestUpdate > CLIENT_TIME_OUT) {
                if (null == clientsToRemove) clientsToRemove = new List<NetworkClientInfo>();
                clientsToRemove.Add(clientInfo);
            }
        }

        if(null != clientsToRemove)
            foreach (NetworkClientInfo clientToRemove in clientsToRemove){
                discoveredClients.Remove(clientToRemove);
                NotifyClientLeft();
                //Debug.Log("NetworkDiscovery: Removed timed out client: " + clientToRemove);
            }
    }

    public void SetInfoToBroadcast(LocalClientInfo localClientInfo)
    {
        DebugUtils.AssertNotNull(DebugUtils.GetMemberName(() => localClientInfo), localClientInfo);

        if (!localClientInfo.Equals(this.localClient))
        {
            //listen for local client changes, to update broadcast
            if(null!=localClient) localClient.OnModelChanged -= OnLocalClientChangedHandler;
            localClient = localClientInfo;
            localClient.OnModelChanged += OnLocalClientChangedHandler;
        }

        messageToBroadcast = DiscoveryMessage.GenerateByClientInfo(localClient).ToString();

        //restart service to apply changes
        if (broadcastSender.IsRunning())
        {
            bool success = broadcastSender.SetBroadcastData(messageToBroadcast);
            if (success)
                //broadcastSender.RestartService();
                StartCoroutine(broadcastSender.RestartServiceRoutine());
        }
    }

    public void OnLocalClientChangedHandler()
    {
        //update broadcast message, since playerName or isPlaying changed
        SetInfoToBroadcast(localClient);
    }

    private void HandleBroadcastMessage(string clientIP, int clientPort, string message)
    {
        DiscoveryMessage dcm = DiscoveryMessage.StringToMessage(message);
        if (null == dcm)
            return;
        
        bool clientIsRegistered = false;
        for (int i = 0; !clientIsRegistered && i < discoveredClients.Count; i++)
        {
            if (discoveredClients[i].GetID().Equals(dcm.clientID))
            {
                if (discoveredClients[i].UpdateRequired(dcm.clientName, dcm.isPlaying, clientIP))
                {
                    discoveredClients[i].Update(dcm.clientName, dcm.isPlaying, clientIP);
                    NotifyClientUpdated();
                }
                discoveredClients[i].UpdateTimer();
                clientIsRegistered = true;

                break;
            }
        }

        if (!clientIsRegistered && !dcm.clientID.Equals(localClient.GetID()))
        {
            NetworkClientInfo clientInfo = new NetworkClientInfo(dcm.clientID, dcm.clientName, dcm.isPlaying, clientIP);
            discoveredClients.AddSorted(clientInfo);
            NotifyClientJoined();
            //Debug.Log("NetworkDiscovery: Discovered new client: " + clientInfo);
        }

        //Debug.Log("NetworkDiscovery: Received Broadcast: [" + clientIP + ":" + clientPort + "] - " + message);
    }

    private void NotifyClientJoined()
    {
        foreach (INetworkChangedHandler handler in onNetworkChangedHandlers)
            handler.OnClientJoined();
    }

    private void NotifyClientLeft()
    {
        foreach (INetworkChangedHandler handler in onNetworkChangedHandlers)
            handler.OnClientLeft();
    }

    private void NotifyClientUpdated()
    {
        foreach (INetworkChangedHandler handler in onNetworkChangedHandlers)
            handler.OnClientUpdated();
    }
}
