﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class LobbyPlayer : NetworkLobbyPlayer {

    private string LOG_PREFIX = typeof(LobbyPlayer).Name + ": ";

    private static UILoadingIndicator returningToLobbyIndicator;
    private static UITextFlasher countdownFlasher;
    private UILobbyPlayer lobbyPlayerUI;
    private LobbyController lobby;

    [SyncVar(hook="OnNameSync"), SerializeField]
    private string playerName;

    [SyncVar(hook = "OnColorSync"), SerializeField]
    private Color playerColor = Color.clear;

    [SyncVar(hook ="OnIsInLobbySync")]
    private bool isInLobby;

    private bool localSetupComplete = false;

    public string GetPlayerName()
    {
        return playerName;
    }

    public Color GetPlayerColor()
    {
        return playerColor;
    }

    public UILobbyPlayer GetUI()
    {
        return lobbyPlayerUI;
    }

    public bool IsInLobby()
    {
        return isInLobby;
    }

    //is called when the lobbyplayer enters the lobby and afterwards every time the lobby scene is loaded by the lobbymanager
    //while this player is still connected
    //Note: isLocalPlayer is not guaranteed to be set until OnStartLocalPlayer is called.
    public override void OnClientEnterLobby()
    {
        Debug.Log(LOG_PREFIX + "OnClientEnterLobby | NetID:" + netId);

        //hide default ui and instantiate custom one
        ShowLobbyGUI = false;
        if(null==lobbyPlayerUI)
            lobbyPlayerUI = UIElement.Instantiate<UILobbyPlayer>();

        lobby = (LobbyController)Object.FindObjectOfType(typeof(LobbyController));
        lobby.AddLobbyPlayer(this);

        //ensure to update gui in case we got a client from the server that has already called CMDs before we joined
        OnNameSync(playerName);
        OnColorSync(playerColor);
        OnClientReady(readyToBegin);


        if (isLocalPlayer)
        {
            //make local player controls interactable and notify Server about client entered lobby
            lobbyPlayerUI.AllowInteraction();
            lobby.GetUI().GetReadyButton().interactable = true;
            Debug.Log(LOG_PREFIX + " invoking CmdEnteredLobby...");
            CmdEnteredLobby();
        }
        else
            lobbyPlayerUI.PreventInteraction();
    }

    public override void OnStartLocalPlayer()
    {
        Debug.Log(LOG_PREFIX + "OnStartLocalPlayer | NetID:" + netId);
        base.OnStartLocalPlayer();

        if (localSetupComplete)
            return;

        SetupLocalPlayer();
        Debug.Log(LOG_PREFIX + " invoking CmdEnteredLobby...");
        CmdEnteredLobby();
    }

    public void SetupLocalPlayer()
    {
        //update playername on server. 
        if(string.IsNullOrEmpty(playerName))
            CmdChangeName(GameManager.GetInstance().GetPlayerInfo().GetPlayerName());

        //update playercolor on server
        if(Color.clear.Equals(playerColor))
            CmdChangeColor();

        //this is a local player, so make the UI of this player interactable.
        lobbyPlayerUI.SetLocalCallbacks(OnColorClicked, OnReadyClicked);
        lobbyPlayerUI.AllowInteraction();
        lobby.GetUI().GetReadyButton().interactable = true;
        lobby.GetUI().GetChatUI().SetSendCallback((messageText) => { CmdSendChatMessage(playerName, messageText); });
        lobby.GetUI().SetReadyButtonAction(() => { OnReadyClicked(!readyToBegin);});

        //listen for localClientInfo change, to update name of local lobby player
        LocalClientInfo localClientInfo = GameManager.GetInstance().GetPlayerInfo();
        localClientInfo.OnModelChanged += OnLocalClientInfoChanged;

        localSetupComplete = true;
    }

    private void OnLocalClientInfoChanged()
    {
        LocalClientInfo localClientInfo = GameManager.GetInstance().GetPlayerInfo();
        if (!localClientInfo.GetPlayerName().Equals(playerName))
            CmdChangeName(localClientInfo.GetPlayerName()); 
    }

    //Cleanup when player is destroyed (which happens when client kicked or disconnected)
    public void OnDestroy()
    {
        Debug.Log(LOG_PREFIX + "OnDestroy()");

        lobby.RemoveLobbyPlayer(this);
        Destroy(lobbyPlayerUI.gameObject);

        //deregister listener
        if (isLocalPlayer && null!=GameManager.GetInstance())
        {
            LocalClientInfo localClientInfo = GameManager.GetInstance().GetPlayerInfo();
            localClientInfo.OnModelChanged -= OnLocalClientInfoChanged;
        }
    }

    public void OnReadyClicked(bool readyValue)
    {
        if (readyValue)
            SendReadyToBeginMessage();
        else
            SendNotReadyToBeginMessage();
    }


    public void OnColorClicked()
    {
        CmdChangeColor();
    }


    [Command]
    public void CmdChangeName(string name)
    {
        //this will be executed on server and then call OnNameSync hook of the playerName syncvar
        playerName = name;
    }

    //this will be executed on server and then call OnColorSync hook of the playerColor syncvar
    [Command]
    public void CmdChangeColor()
    {
        playerColor = lobby.GetUnusedPlayerColor(playerColor);
    }

    //this is triggered by the client and executed by the server. it causes the server to forward the given chatmessage to all clients.
    [Command]
    public void CmdSendChatMessage(string senderName, string messageText)
    {
        //forward message to clients
        RpcReceiveChatMessage(senderName, messageText);
    }

    [Command]
    public void CmdEnteredLobby()
    {
        Debug.Log(LOG_PREFIX + "CmdEnteredLobby");
        isInLobby = true;
        lobby.OnLobbyServerPlayerEnteredLobby(this);
    }

    //this is triggered by the server and executed on all the clients (but always on the same player object as it was sent from).
    //it adds a chat message to the chat ui. also it checks if the sender client is the localClient 
    //determine the alignment of the message (left for remote, right for local).
    [ClientRpc]
    public void RpcReceiveChatMessage(string senderName, string messageText)
    {
        Debug.Log(LOG_PREFIX + "RpcReceiveChatMessage - from: "+senderName +" - msg: "+messageText);
        lobby.GetUI().GetChatUI().AddChatMessage(senderName, messageText, isLocalPlayer);
    }

    [ClientRpc]
    public void RpcUpdateStartCountdown(int countdown)
    {
        if (!isLocalPlayer)
            return;

        Debug.Log(LOG_PREFIX + "RpcUpdateStartCountdown: " + countdown.ToString());

        //if countdown is active, player UI should be non interactable
        lobbyPlayerUI.PreventInteraction();
        lobby.GetUI().GetReadyButton().interactable = false;

        if (null == countdownFlasher)
            countdownFlasher = InitCountDownFlasher();
        StartCoroutine(countdownFlasher.Flash(countdown.ToString()));
    }

    private UITextFlasher InitCountDownFlasher()
    {
        UITextFlasher flasher = UIElement.Instantiate<UITextFlasher>(null, "CountDownFlasher");
        flasher.SetFadeSpeed(10);
        flasher.SetStayDuration(0.5f);
        flasher.SetBlockRaycasts(true);
        flasher.SetFontSize(300);
        return flasher;
    }

    [ClientRpc]
    public void RpcReturningToLobby()
    {
        Debug.Log(LOG_PREFIX + "RpcReturningToLobby");
        if (null == returningToLobbyIndicator)
            returningToLobbyIndicator = InitReturningToLobbyIndicator();
        returningToLobbyIndicator.Show();
    }

    [ClientRpc]
    public void RpcAllReturnedToLobby()
    {
        Debug.Log(LOG_PREFIX + "RpcAllReturnedToLobby");

        if (null == returningToLobbyIndicator)
            returningToLobbyIndicator = InitReturningToLobbyIndicator();
        returningToLobbyIndicator.Hide();
    }

    private UILoadingIndicator InitReturningToLobbyIndicator()
    {
        this.transform.position = new Vector3(0, 0, 0);
        UILoadingIndicator indicator = UIElement.Instantiate<UILoadingIndicator>(this.transform,"ReturningToLobbyIndicator");
        indicator.SetIndicationText(Global.STRING_INDICATE_RETURNING_TO_LOBBY);
        indicator.SetIsCancelAble(false);
        indicator.Hide();
        return indicator;
    }

    public void OnNameSync(string newName)
    {
        if (string.IsNullOrEmpty(newName))
            return;
        LOG_PREFIX = typeof(LobbyPlayer).Name + " (" + newName + "): ";
        playerName = newName;
        lobbyPlayerUI.SetPlayerName(playerName);
    }

    public void OnColorSync(Color newColor)
    {
        playerColor = newColor;
        lobbyPlayerUI.SetPlayerColor(newColor);
    }

    public void OnIsInLobbySync(bool inLobby)
    {
        this.isInLobby = inLobby;
    }

    public override void OnClientReady(bool readyState)
    {
        Debug.Log(LOG_PREFIX + "OnClientReady - ready=" + readyState);

        //update ready toggle button ui to indicate player is ready
        lobbyPlayerUI.SetReadyToggleState(readyState);

        //lock / unlock UI for local player to prevent changes while ready
        if(!isLocalPlayer)
            return;

        lobbyPlayerUI.ToggleColorButton(!readyState);
        lobbyPlayerUI.TogglePlayerName(!readyState);
        lobby.GetUI().GetBackButton().interactable = !readyState;
        lobby.GetUI().GetSettingsButton().interactable = !readyState;
        lobby.GetUI().GetLeaveButton().interactable = !readyState;
        lobby.GetUI().GetPlayerButton().interactable = !readyState;

        if (!readyState)
            lobby.GetUI().GetReadyButton().interactable = true;
    }

    public override string ToString()
    {
        return playerName;
    }
}
