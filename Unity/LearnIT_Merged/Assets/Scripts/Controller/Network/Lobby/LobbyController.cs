﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class LobbyController : NetworkLobbyManager {

    private static readonly Color[] PLAYER_COLORS = new Color[] { Color.blue, Color.red, Color.green, Color.magenta, Global.COLOR_PINK, Color.yellow, Color.cyan };
    private static readonly string LOG_PREFIX = typeof(LobbyController).Name+": ";
    public const int COUNTDOWN_TO_START_MATCH = 3;

    //make sure this is conform with the channels added to the lobbymanager in inspector!
    public const int CHANNEL_RELIABLE = 0;
    public const int CHANNEL_UNRELIABLE = 1;
    public const int CHANNEL_RELIABLE_SEQUENCED = 2;

    public event UnityAction onLobbyClientDisconnect; //being invoked when we are client and the lobby shuts down
    public event UnityAction onLobbyServerDisconnect; //being invoked when we are hosting the lobby and a client leaves us
    public event UnityAction onLobbyLeaveButton;

    private UILoadingIndicator waitForPlayersIndicator;
    private UILobby lobbyUI;
    private Dictionary<uint, LobbyPlayer> lobbyPlayers;
    private GameMode gameModeToPlay;
    private LocalClientInfo localClientInfo;
    private AudioPlayer audioPlayer;

    public void Setup(GameMode gameModeToPlay)
    {
        Debug.Log(LOG_PREFIX + "Setup "+gameModeToPlay.GetName());
        this.gameModeToPlay = gameModeToPlay;

        //hide default ui and instantiate custom one
        showLobbyGUI = false;
        lobbyUI = UIElement.Instantiate<UILobby>(gameObject.transform);
        lobbyUI.SetGameModeTitle(gameModeToPlay.GetName());
        lobbyUI.SetGameModeDescription(gameModeToPlay.GetDescription());
        lobbyUI.SetLeaveButtonAction(InvokeLeaveLobbyButton);

        //listen for localClientInfo change, to update name of local player
        localClientInfo = GameManager.GetInstance().GetPlayerInfo();
        localClientInfo.OnModelChanged += OnLocalPlayerNameChanged;
        OnLocalPlayerNameChanged();

        //show loading indicator until all players are available (alternatively disable backbutton until players are connected)
        this.transform.position = new Vector3(0, 0, 0);
        waitForPlayersIndicator = UIElement.Instantiate<UILoadingIndicator>(this.transform,"WaitForPlayersIndicator");
        waitForPlayersIndicator.SetIndicationText(Global.STRING_INDICATE_WAITING_FOR_PLAYERS);
        waitForPlayersIndicator.SetIsCancelAble(false);
        waitForPlayersIndicator.Show();

        lobbyPlayers = new Dictionary<uint, LobbyPlayer>();
        playScene = gameModeToPlay.GetGameSceneName();
        gamePlayerPrefab = Resources.Load<GameObject>(gameModeToPlay.GetMPGamePlayerPrefabPath());
        audioPlayer = gameObject.AddComponent<AudioPlayer>();
    }

    public GameMode GetGameModeToPlay()
    {
        return gameModeToPlay;
    }

    public void AddLobbyPlayer(LobbyPlayer lp)
    {
        Debug.Log(LOG_PREFIX + "AddLobbyPlayer. lobbyplayer="+lp);

        //add lobby player to maintaining list and to gui list
        if (null != lp && !lobbyPlayers.ContainsKey(lp.netId.Value))
        {
            lobbyPlayers.Add(lp.netId.Value, lp);
            lobbyUI.AddLobbyPlayerUI(lp.GetUI());
        }

        //check whether required amount of players is reached and hide waiting indicator
        if (lobbyPlayers.Count >= this.minPlayers)
        {
            waitForPlayersIndicator.Hide();
        }
    }

    public void RemoveLobbyPlayer(LobbyPlayer lp)
    {
        Debug.Log(LOG_PREFIX + "RemoveLobbyPlayer. lobbyplayer="+lp);

        if (null == lp || !lobbyPlayers.ContainsKey(lp.netId.Value))
            return;

        //actually remove the leaving player
        lobbyPlayers.Remove(lp.netId.Value);
        lobbyUI.RemoveLobbyPlayerUI(lp.GetUI());
    }

    //called on clients
    public UILobby GetUI()
    {
        return lobbyUI;
    }

    //called on server to tell all clients that server is returning to lobby
    public void InvokeReturnToLobby()
    {
        foreach (LobbyPlayer lp in lobbyPlayers.Values)
        {
            lp.RpcReturningToLobby();
            break;
        }

        base.ServerReturnToLobby();
    }

    //called on server, which knows all the players colors
    private Color[] GetPlayerColors()
    {
        Color[] colors = new Color[lobbyPlayers.Count];
        int i = 0;
        foreach (LobbyPlayer lp in lobbyPlayers.Values)
        {
            colors[i] = lp.GetPlayerColor();
            i++;
        }
        return colors;
    }

    //called on server to determine a currently unused color.
    //iterates through the color array, starting at currentColors position
    public Color GetUnusedPlayerColor(Color currentColor)
    {
        Color[] colorsInUse = GetPlayerColors();
        int currentColorIndex = System.Array.IndexOf(PLAYER_COLORS, currentColor);
        if (currentColorIndex < 0) currentColorIndex = 0;

        bool currentColorInUse = false;
        do
        {
            currentColorInUse = false;
            currentColorIndex = (currentColorIndex + 1) % PLAYER_COLORS.Length;
            foreach (Color color in colorsInUse)
            {
                if (color.Equals(PLAYER_COLORS[currentColorIndex]))
                    currentColorInUse = true;
            }
        } while (currentColorInUse);

        return PLAYER_COLORS[currentColorIndex];
    }

    // ----------------- Server callbacks ------------------
    public override void OnLobbyStartHost()
    {
        Debug.Log(LOG_PREFIX + "OnLobbyStartHost");
        base.OnLobbyStartHost();
    }

    public override void OnLobbyStopHost()
    {
        Debug.Log(LOG_PREFIX+"OnLobbyStopHost");
        base.OnLobbyStopHost();
    }

    //This is called on the server when it is told that a client has finished switching from the lobby scene to a game player scene.
    public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayerObject, GameObject gamePlayerObject)
    {
        Debug.Log(LOG_PREFIX + "OnLobbyServerSceneLoadedForPlayer (" + lobbyPlayerObject.GetComponent<LobbyPlayer>().GetPlayerName() + ")");
        base.OnLobbyServerSceneLoadedForPlayer(lobbyPlayerObject, gamePlayerObject);

        //transfer data from lobbyplayer to gameplayer as soon as its ready for it
        LobbyPlayer lobbyPlayer = lobbyPlayerObject.GetComponent<LobbyPlayer>();
        lobbyPlayer.OnIsInLobbySync(false);
        MPGamePlayer gamePlayer = gamePlayerObject.GetComponent<MPGamePlayer>();
        gamePlayer.SetPlayerName(lobbyPlayer.GetPlayerName());
        gamePlayer.SetPlayerColor(lobbyPlayer.GetPlayerColor());

        return true;
    }

    
    public override GameObject OnLobbyServerCreateGamePlayer(NetworkConnection conn, short playerControllerId)
    {
        Debug.Log(LOG_PREFIX + "OnLobbyServerCreateGamePlayer");
        return base.OnLobbyServerCreateGamePlayer(conn, playerControllerId);
    }

    public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
    {
        Debug.Log(LOG_PREFIX + "OnLobbyServerCreateLobbyPlayer");
        return base.OnLobbyServerCreateLobbyPlayer(conn, playerControllerId);
    }

    public override void OnLobbyServerPlayerRemoved(NetworkConnection conn, short playerControllerId)
    {
        Debug.Log(LOG_PREFIX + "OnLobbyServerPlayerRemoved");
        base.OnLobbyServerPlayerRemoved(conn, playerControllerId);
    }

    public override void OnLobbyServerConnect(NetworkConnection conn)
    {
        Debug.Log(LOG_PREFIX + "OnLobbyServerConnect");
        base.OnLobbyServerConnect(conn);

    }

    //being invoked when we are hosting the lobby and a client leaves us
    //conn = the connection to the client that left / disconnected from our lobby
    public override void OnLobbyServerDisconnect(NetworkConnection conn)
    {
        Debug.Log(LOG_PREFIX + "OnLobbyServerDisconnect");
        base.OnLobbyServerDisconnect(conn);

        if (null != onLobbyServerDisconnect)
        {
            onLobbyServerDisconnect.Invoke();
        }
    }

    public override void OnServerError(NetworkConnection conn, int errorCode)
    {
        Debug.LogError(LOG_PREFIX+"Server Error " + errorCode.ToString());
        base.OnServerError(conn, errorCode);

    }

    //called when all players are ready to start match
    public override void OnLobbyServerPlayersReady()
    {
        Debug.Log(LOG_PREFIX + "OnLobbyServerPlayersReady");

        //start countdown when all players are ready
        StartCoroutine(ServerCountdownToStartRoutine());
    }

    public IEnumerator ServerCountdownToStartRoutine()
    {
        float remainingTime = COUNTDOWN_TO_START_MATCH+1;
        int currentCountDown = COUNTDOWN_TO_START_MATCH+1;

        while (currentCountDown > 0)
        {
            yield return null;

            //to avoid flooding the network of message, we only inform client when the number of plain seconds change.
            int newFloorTime = Mathf.FloorToInt(remainingTime);
            if (newFloorTime != currentCountDown)
            {
                currentCountDown = newFloorTime;
                foreach(LobbyPlayer lp in lobbyPlayers.Values)
                {
                    lp.RpcUpdateStartCountdown(currentCountDown);
                }
            }

            remainingTime -= Time.deltaTime;
        }

        if(!Application.loadedLevelName.Equals(playScene))
            ServerChangeScene(playScene);
    }

    //called on the server when the local lobby player of a client finished entering lobby
    public void OnLobbyServerPlayerEnteredLobby(LobbyPlayer lobbyPlayer)
    {
        Debug.Log(LOG_PREFIX + "OnLobbyPlayerEnteredLobby lobbyplayer=" + lobbyPlayer);

        //check whether all players entered lobby already...
        if (lobbyPlayers.Values.Count < minPlayers)
            return;

        bool allEntered = true;
        foreach (LobbyPlayer player in lobbyPlayers.Values)
        {
            if (!player.IsInLobby())
            {
                allEntered = false;
                break;
            }
        }
        
        //and notify clients, if so...
        if (allEntered)
        {
            OnLobbyServerAllPlayersEnteredLobby();
        }
    }

    public void OnLobbyServerAllPlayersEnteredLobby()
    {
        Debug.Log(LOG_PREFIX + "OnLobbyAllPlayersEnteredLobby");
        
        //notify clients about "all lobbyplayers entered lobby"
        //we do this in coroutine, since we have to wait for lobbyplayer objects to be ready to receive
        StartCoroutine(NotifyAllReturnedToLobby());
    }
    
    private IEnumerator NotifyAllReturnedToLobby()
    {
        foreach (LobbyPlayer lp in lobbyPlayers.Values)
        {           
            yield return null;
            lp.RpcAllReturnedToLobby();
            break;
        }
    }

    // ----------------- Server callbacks End ------------------

    // ----------------- Client callbacks Begin ------------------
    public override void OnLobbyStopClient()
    {
        Debug.Log(LOG_PREFIX + "OnLobbyStopClient");
        base.OnLobbyStopClient();
    }

    public override void OnLobbyClientConnect(NetworkConnection conn)
    {
        Debug.Log(LOG_PREFIX + "OnLobbyClientConnect");
        base.OnLobbyClientConnect(conn);
    }

    //being invoked when we are client and the lobby shuts down / kicks us
    //conn = the connection from us as local client to the server which was hosting the lobby
    public override void OnLobbyClientDisconnect(NetworkConnection conn)
    {
        Debug.Log(LOG_PREFIX+"OnLobbyClientDisconnect");
        base.OnLobbyClientDisconnect(conn);

        if (null != onLobbyClientDisconnect)
        {
            onLobbyClientDisconnect.Invoke();
        }
    }

    public override void OnClientError(NetworkConnection conn, int errorCode)
    {
        Debug.LogError(LOG_PREFIX+"Client Error " + errorCode.ToString());
        base.OnClientError(conn, errorCode);
    }

    

    public override void OnLobbyStartClient(NetworkClient client)
    {
        Debug.Log(LOG_PREFIX+"OnLobbyStartClient");
        base.OnLobbyStartClient(client);
    }

    public override void OnLobbyClientAddPlayerFailed()
    {
        Debug.Log(LOG_PREFIX+"OnLobbyClientAddPlayerFailed");
        base.OnLobbyClientAddPlayerFailed();
    }

    public override void OnLobbyClientEnter()
    {
        Debug.Log(LOG_PREFIX+"OnLobbyClientEnter");
        base.OnLobbyClientEnter();
    }

    public override void OnLobbyClientExit()
    {
        Debug.Log(LOG_PREFIX+"OnLobbyClientExit");
        base.OnLobbyClientExit();
    }

    //Called on clients when a scene has completed loading, if the scene load was initiated by the server.
    //Scene changes can cause player objects to be destroyed. 
    //The default implementation of OnClientSceneChanged in the NetworkManager is to add a player object for the connection if no player object exists.
    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        Debug.Log(LOG_PREFIX + "OnClientSceneChanged");
        base.OnClientSceneChanged(conn);

        //when entering lobbyScene, show lobby UI
        if (Application.loadedLevelName == lobbyScene)
        {
            lobbyUI.gameObject.SetActive(true);
            audioPlayer.PlayMenuMusic();
        }
        else
        {
            lobbyUI.gameObject.SetActive(false);
            audioPlayer.StopBgMusic();
        }
    }
    // ----------------- Client callbacks End ------------------

    private void OnLocalPlayerNameChanged()
    {
        lobbyUI.GetMenuHeader().SetPlayerName(localClientInfo.GetPlayerName());
    }

    void OnDestroy()
    {
        //deregister listener
        if (null != GameManager.GetInstance() && null != localClientInfo)
        {
            localClientInfo.OnModelChanged -= OnLocalPlayerNameChanged;
        }
    }

    public void InvokeLeaveLobbyButton()
    {
        if (null != onLobbyLeaveButton) 
            onLobbyLeaveButton.Invoke();
    }
}
