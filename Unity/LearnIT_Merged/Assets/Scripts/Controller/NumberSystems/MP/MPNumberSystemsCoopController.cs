﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections.Generic;

public class MPNumberSystemsCoopController : MPGameController
{
    protected MPUINumberSystemsCOOP mpNumberSystemsCoopUI;
    MPUINumberSystemsCoopOverview gameOverview;
    MPNumberSystemsCoopPlayer player;

    private NumberCreator numberCreator;
    private UICountSlider slider;

    private string leftText;
    private string rightText;
    private string topText;
    protected int level = 1;
    private Dictionary<uint, string> answers = new Dictionary<uint, string>();
    private uint leftPlayerID;
    private int triesLeft = Global.MAX_TRIES_COOP;
	private int swapsLeft = Global.MAX_SWAPS_COOP;
	private uint? swapRequester = null;
	private LinkedList<Jokercard> jokerCards = new LinkedList<Jokercard> ();
	private NumberSystem[] systems = new NumberSystem[]{new NumberSystem(2, Global.BINARY_TEXT), new NumberSystem(8, Global.OCTAL_TEXT), new NumberSystem(16, Global.HEX_TEXT), new NumberSystem(10, Global.DECIMAL_TEXT)};

    [SerializeField]
    private int numberSystemTop = 10;
    [SerializeField]
    private int numberSystemLeft = 2;
    [SerializeField]
    private int numberSystemRight = 2;
    [SerializeField]
    protected string numberSystemTopName = Global.DECIMAL_TEXT;
    [SerializeField]
    protected string numberSystemLeftName = Global.BINARY_TEXT;
    [SerializeField]
    protected string numberSystemRightName = Global.BINARY_TEXT;
    [SerializeField]
    private float difficultyScalingDez = 1.68F;   
	[SerializeField]
	private float stretchScalingDez = 9.25F;
	[SerializeField]
	private float difficultyScalingBin = 1.12F;   
	[SerializeField]
	private float stretchScalingBin = 13.05F;
	[SerializeField]
	private float difficultyScalingOct = 2.16F;   
	[SerializeField]
	private float stretchScalingOct = 1.85F;
	[SerializeField]
	private float difficultyScalingHex = 1.68F;   
	[SerializeField]
	private float stretchScalingHex = 9.25F;

    protected override void InitBothSides()
    {
    }

    protected virtual NumberCreator GetNumberCreator()
    {
		return new NumberCreatorCoop(numberSystemLeft, numberSystemRight, numberSystemTop);
    }

	/**
	 * Randomly selects 3 out of 4 preset numbersystems for the game.
	 */
	[Server]
	private void GenerateNumberSystem()
	{
		System.Random rand = new System.Random ();

		ArrayList list = new ArrayList (systems);

		int iFirst = rand.Next (0, 4);
		int iSecond = rand.Next (0, 3);
		int iThird = rand.Next (0, 2);

		NumberSystem topSystem = ((NumberSystem)list [iFirst]);
		list.RemoveAt (iFirst);

		NumberSystem leftSystem = ((NumberSystem)list [iSecond]);
		list.RemoveAt (iSecond);

		NumberSystem rightSystem = ((NumberSystem)list[iThird]);

		numberSystemTop = topSystem.Number;
		numberSystemTopName = topSystem.Name;

		numberSystemRight = rightSystem.Number;
		numberSystemRightName = rightSystem.Name;

		numberSystemLeft = leftSystem.Number;
		numberSystemLeftName = leftSystem.Name;
	}

	/**
	 * Sets the displayed texts for the chosen numbersystems.
	 */
	[ClientRpc]
	private void RpcSetNumberSystems(uint leftPlayerID, string leftName, string rightName, string topName)
	{
		if (GetLocalGamePlayer ().netId.Value != leftPlayerID) {
			mpNumberSystemsCoopUI.SetLeftSystemName (leftName);
			mpNumberSystemsCoopUI.SetRightSystemName (rightName);
			mpNumberSystemsCoopUI.SetUpperSystemName (topName);

		} else {
			mpNumberSystemsCoopUI.SetLeftSystemName (rightName);
			mpNumberSystemsCoopUI.SetRightSystemName (leftName);
			mpNumberSystemsCoopUI.SetUpperSystemName (topName);
		}
	}

    protected override void InitClientSide()
    {
        mpNumberSystemsCoopUI = UIElement.Instantiate<MPUINumberSystemsCOOP>();
		mpNumberSystemsCoopUI.SetNumberSystemController (this);
        slider = UIElement.Instantiate<UICountSlider>();
        mpNumberSystemsCoopUI.setSlider(slider);
		slider.Reset (Global.MAX_TRIES_COOP);
		slider.SetAppendingText (" /  " + Global.MAX_TRIES_COOP);
		slider.SetIsCountingDown (true);


        Button submitButton = mpNumberSystemsCoopUI.GetSubmitButton();
        mpNumberSystemsCoopUI.GetHeader().SetLevelCountText(level.ToString());
        submitButton.onClick.AddListener(OnSubmitListener);

		Button swapButton = mpNumberSystemsCoopUI.GetSwapButton ();
		swapButton.onClick.AddListener (OnSwapListener);

        mpNumberSystemsCoopUI.SetLeftSystemName(numberSystemLeftName);
        mpNumberSystemsCoopUI.SetRightSystemName(numberSystemRightName);
        mpNumberSystemsCoopUI.SetUpperSystemName(numberSystemTopName);
        mpNumberSystemsCoopUI.GetHeader().SetTitleText(Global.NUMBERSYSTEMS_COOP_TITLE_TEXT);
		mpNumberSystemsCoopUI.GetNumPad ().AddOnSubmitAction (OnSubmitListener);
		for (int i = 0; i < Global.MAX_SWAPS_COOP; i++)
		{
			AddJoker ();
		}

		mpNumberSystemsCoopUI.GetInputDone ().enabled = false;
		mpNumberSystemsCoopUI.GetInputWaiting ().enabled = false;
		mpNumberSystemsCoopUI.GetTextWaiting ().enabled = false;
		mpNumberSystemsCoopUI.GetLocalDone ().enabled = false;
		mpNumberSystemsCoopUI.GetLocalWrong ().enabled = false;
		mpNumberSystemsCoopUI.GetInputSwap ().enabled = false;
		mpNumberSystemsCoopUI.GetSwapButton ().gameObject.SetActive(false);

        mpNumberSystemsCoopUI.AddPauseButtonAction(TriggerLeaveGame);

    }

	[Client]
	private void TriggerLeaveGame(){
		((MPNumberSystemsCoopPlayer)GetLocalGamePlayer ()).CmdLeaveGame ();
	}

	[Server]
	public void LeaveGame()
	{
		lobby.InvokeReturnToLobby();
	}

    protected override void InitServerSide()
    {
		CreateNumberSystems ();
    }

	private void CreateNumberSystems()
	{
		GenerateNumberSystem ();
		numberCreator = GetNumberCreator();
	}

    [Server]
    protected override void OnAllPlayersReadyToBeginMatch()
	{
		ArrayList players = new ArrayList();
		foreach (KeyValuePair<uint, MPGamePlayer> player in gamePlayers)
		{
			players.Add(player.Key);
		}
		leftPlayerID = (uint)players[0];
		CreateNumberSystems ();
		RpcSetNumberSystems (leftPlayerID, numberSystemLeftName, numberSystemRightName, numberSystemTopName);

        CreateNumber();
        RpcInitGui();
        StartMatch();
    }

    [ClientRpc]
    private void RpcInitGui()
    {
        player = (MPNumberSystemsCoopPlayer)GetLocalGamePlayer();
    }

    protected override void OnMatchFinished()
    {
        gameOverview.ResultText.text = Global.END_TEXT_NUMBERSYSTEMS_COOP_0 + level + Global.END_TEXT_NUMBERSYSTEMS_COOP_1;
        gameOverview.ResultImage.sprite = Resources.Load<Sprite>(Global.PATH_ICON_WIN);
    }

    protected override void OnMatchStarted()
    {
    }

	/**
	 * Calculates the amount of digits dependant on the chosen numbersystem and the assotiated strechScaling and difficultyScaling and the current level.
	 */
    [Server]
    protected int getDigits()
	{
		float stretchScaling = 1;
		float difficultyScaling = 1;
		switch(numberSystemTop)
		{
		case(2):
			stretchScaling = stretchScalingBin;
			difficultyScaling = difficultyScalingBin;
			break;
		case(8):
			stretchScaling = stretchScalingOct;
			difficultyScaling = difficultyScalingOct;
			break;
		case(10):
			stretchScaling = stretchScalingDez;
			difficultyScaling = difficultyScalingDez;
			break;
		case(16):
			stretchScaling = stretchScalingHex;
			difficultyScaling = difficultyScalingHex;
			break;

		}
		return (int)(Mathf.Log(level + stretchScaling - 1)/Mathf.Log(difficultyScaling) + 1 - Mathf.Log(stretchScaling)/Mathf.Log(difficultyScaling));
    }

	/**
	 * Sends the user-input to the server, blocks every further user-input and displays a waiting symbol.
	 */
    [Client]
    private void OnSubmitListener()
    {
		mpNumberSystemsCoopUI.GetNumPad ().enabled = false;
		mpNumberSystemsCoopUI.Unlocked = false;
		mpNumberSystemsCoopUI.GetSubmitButton ().interactable = false;
		string message = mpNumberSystemsCoopUI.GetInputFieldRight().text;
        player.CmdSubmitResult(message);
		ShowInputWaiting ();
	
    }

	/**
	 * Checks if the supplied result is correct.
	 */
    [Server]
    public void OnResultReceived(string message, uint playerID)
    {
        answers[playerID] = message;
        if (answers.Count < 2)
        {
			RpcShowInputForClientDone (playerID);
            return;
        }
		LinkedList<uint> wrongAnswers = new LinkedList<uint> ();
        foreach (KeyValuePair<uint, string> content in answers)
        {
            if (content.Key == leftPlayerID)
            {
                if (content.Value != leftText)
                {
					wrongAnswers.AddLast(content.Key);
                }
            }
            else
            {
                if (content.Value != rightText)
                {
					wrongAnswers.AddLast(content.Key);
                }
            }
        }

		uint[] wrongAnswersArray = new uint[wrongAnswers.Count];
		int i = 0;
		foreach(uint id in wrongAnswers)
		{
			wrongAnswersArray [i++] = id;
			answers.Remove (id);
		}
		RpcSendFeedBack(wrongAnswersArray);
        CheckMatchFinished(wrongAnswers.Count);
        if (wrongAnswers.Count == 0)
        {
			answers.Clear();
			RpcUpdateLevelGui (level);
			RpcResetSlider ();
			RpcResetTextGui ();
			RpcResetSwap ();
			CreateNumberSystems ();
			RpcSetNumberSystems (leftPlayerID, numberSystemLeftName, numberSystemRightName, numberSystemTopName);
            CreateNumber();
			triesLeft = Global.MAX_TRIES_COOP;
			swapRequester = null;
        }
    }

	[ClientRpc]
	private void RpcResetTextGui()
	{
		mpNumberSystemsCoopUI.GetInputFieldRight ().text = "";
		mpNumberSystemsCoopUI.GetInputFieldLeft ().text = "";
		mpNumberSystemsCoopUI.Unlocked = true;
		mpNumberSystemsCoopUI.GetNumPad ().enabled = true;
	}

	[ClientRpc]
	private void RpcResetSwap()
	{
		HideInputPartner ();
	}


    [Server]
	protected virtual void CheckMatchFinished(int wrongAnswers)
    {
		if (wrongAnswers == 0)
        {
            if (Global.MAX_LEVEL <= level)
            {
                FinishMatch();
            }
            else
            {
                level++;
            }
        }
        else
        {
			triesLeft -= 1;
            if (triesLeft <= 0)
            {
                FinishMatch();
            }
        }
    }
		
	[ClientRpc]
	private void RpcUpdateLevelGui(int level)
	{
		this.level = level;
		mpNumberSystemsCoopUI.GetHeader().SetLevelCountText(level.ToString());
	}

	[ClientRpc]
	private void RpcResetSlider()
	{
		slider.Reset ();
	}

    [Server]
    public void CreateNumber()
    {
		string[] result = numberCreator.createNumbers(getDigits());
		Debug.LogError (numberCreator.getLeftSystem () + " " + numberCreator.getRightSystem() + " " + ((NumberCreatorCoop)numberCreator).getTopSystem());
        leftText = result[1];
        rightText = result[2];
        topText = result[0];
        RpcSendNumber(topText);
    }

    [ClientRpc]
	public void RpcSendFeedBack(uint[] wrongAnswers)
    {
		switch(wrongAnswers.Length)
        {
		case (0):
			StartCoroutine (mpNumberSystemsCoopUI.GetScreenFlasher ().Flash (Global.COLOR_FLASH_GREEN));
			mpNumberSystemsCoopUI.GetSwapButton ().gameObject.SetActive(false);
			HideInputPartner ();
			HideInputLocal ();
			HideInputSwap ();
            break;

		case (1):
			slider.Decrement ();
            if (this.player.netId.Value == (uint)wrongAnswers[0])
            {
                StartCoroutine(mpNumberSystemsCoopUI.GetScreenFlasher().Flash(Global.COLOR_FLASH_RED));
				mpNumberSystemsCoopUI.Unlocked = true;
				mpNumberSystemsCoopUI.GetNumPad ().enabled = true;
				mpNumberSystemsCoopUI.GetSwapButton ().gameObject.SetActive (true);
				ShowInputDoneGreen ();
				ShowInputLocalWrong ();
            }
            else
            {
                StartCoroutine(mpNumberSystemsCoopUI.GetScreenFlasher().Flash(new Color(1, 1, 0)));
				ShowInputWaiting ();
				ShowInputLocalDone ();
            }
            break;
		case (2):
			slider.Decrement ();
			StartCoroutine (mpNumberSystemsCoopUI.GetScreenFlasher ().Flash (Global.COLOR_FLASH_RED));
			mpNumberSystemsCoopUI.Unlocked = true;
			mpNumberSystemsCoopUI.GetNumPad ().enabled = true;
            mpNumberSystemsCoopUI.GetSwapButton().gameObject.SetActive(true);
            HideInputPartner ();
			ShowInputLocalWrong ();
            break;
        }
    }

    [ClientRpc]
    public void RpcSendNumber(string decimalNumber)
    {
        mpNumberSystemsCoopUI.GetInputFieldTop().text = decimalNumber;
    }

	[Client]
	private void OnSwapListener()
	{
		this.mpNumberSystemsCoopUI.Waiting = false;
		this.player.CmdRequestSwap ();
	}

	[Server]
	public void OnSwapRequest(uint requester)
	{
		if (swapsLeft > 0) {
			if (swapRequester != null) {
				if (swapRequester == (uint?)requester) {
					swapRequester = null;
					RpcSignalRevokeSwapRequest (requester);
				} else if(answers.Count == 1){
					swapsLeft--;
					answers.Clear ();
					answers.Add ((uint)swapRequester, requester == leftPlayerID ? leftText : rightText);
					Swap (ref rightText, ref leftText);
					Debug.LogError ("LeftSys: " + numberSystemLeftName + " rightSys: " + numberSystemRightName);
					Debug.LogError ("LeftAnswer: " + leftText + " rightAnswer: " + rightText);
					RpcSendSwap (requester);

					swapRequester = null;
				}
                else
                {
                    swapsLeft--;
                    answers.Clear();
                    Swap(ref rightText, ref leftText);
                    RpcSendSwapAllWrong(requester);
                    swapRequester = null;
                }
			} else {
				swapRequester = requester;
				RpcSignalSwapRequest ((uint)requester);
               
            }
		}
		else
		{
			RpcSignalNoSwapsLeft (requester);
		}
	}

	[ClientRpc]
	private void RpcSignalSwapRequest(uint requester)
	{
		if (GetLocalGamePlayer ().netId.Value == requester)
		{
			ShowInputSwap ();
		}
		else
		{
			mpNumberSystemsCoopUI.GetSwapButton ().gameObject.SetActive (true);
			StartCoroutine ("FlashWaiting");
		}
	}

	[ClientRpc]
	private void RpcSignalRevokeSwapRequest(uint requester)
	{
		//revert to default button settings
		this.mpNumberSystemsCoopUI.Waiting = false;
		if (requester == GetLocalGamePlayer ().netId.Value) {
			HideInputSwap ();
		} else {
			mpNumberSystemsCoopUI.GetSwapButton ().gameObject.SetActive(false);
		}

	}

	[ClientRpc]
	private void RpcSignalNoSwapsLeft(uint requester)
	{
		if (GetLocalGamePlayer ().netId.Value == requester)
		{
			StartCoroutine ("FlashRedFast");
		}
	}

	/**
	 * makes the swap button flash yellow and orang.
	 */
	private IEnumerator FlashWaiting()
	{
		mpNumberSystemsCoopUI.GetTextWaiting ().enabled = true;
		Color defaultNormalColor = mpNumberSystemsCoopUI.GetSwapButton ().colors.normalColor;
		Color defaultHighLighted = mpNumberSystemsCoopUI.GetSwapButton ().colors.highlightedColor;
		ColorBlock block = this.mpNumberSystemsCoopUI.GetSwapButton ().colors;
		this.mpNumberSystemsCoopUI.Waiting = true;
		while (this.mpNumberSystemsCoopUI.Waiting)
		{
			block.normalColor = new Color (1.0F, 0.5F, 0);
			block.highlightedColor = new Color (1.0F, 0.5F, 0);
			mpNumberSystemsCoopUI.GetSwapButton ().colors = block;
			yield return new WaitForSeconds (0.75F);
			block.normalColor = new Color(1.0F, 0.85F, 0F);
			block.highlightedColor = new Color (1.0F, 0.85F, 0F);
			mpNumberSystemsCoopUI.GetSwapButton ().colors = block;
			yield return new WaitForSeconds (0.75F);
		}
		block.normalColor = defaultNormalColor;
		block.highlightedColor = defaultHighLighted;
		mpNumberSystemsCoopUI.GetSwapButton ().colors = block;
	}

	/**
	 * Makes the swap button flash red 3 times
	 */
	private IEnumerator FlashRedFast()
	{
		Color defaultNormalColor = mpNumberSystemsCoopUI.GetSwapButton ().colors.normalColor;
		Color defaultHighLightedColor = mpNumberSystemsCoopUI.GetSwapButton ().colors.highlightedColor;
		ColorBlock block = mpNumberSystemsCoopUI.GetSwapButton ().colors;
		for (int i = 0; i < 3; i++)
		{
			block.normalColor = Color.red;
			block.highlightedColor = Color.red;
			mpNumberSystemsCoopUI.GetSwapButton ().colors = block;
			yield return new WaitForSeconds (0.1F);
			block.normalColor = defaultNormalColor;
			block.highlightedColor = defaultHighLightedColor;
			mpNumberSystemsCoopUI.GetSwapButton ().colors = block;
			yield return new WaitForSeconds (0.1F);
		}
	}

	private void Swap<O>(ref O left, ref O right)
	{
		O tmp = left;
		left = right;
		right = tmp;
	}

	/**
	 * Signals that both player supplied a wrong answer.
	 */
    [ClientRpc]
    private void RpcSendSwapAllWrong(uint resolver)
    {
        RemoveJoker();
        if (GetLocalGamePlayer().netId.Value == resolver)
        {
            this.mpNumberSystemsCoopUI.GetInputFieldLeft().text = this.mpNumberSystemsCoopUI.GetInputFieldRight().text;      
        }
        else
        {
            this.mpNumberSystemsCoopUI.GetInputFieldRight().text = this.mpNumberSystemsCoopUI.GetInputFieldLeft().text;
            HideInputSwap();
        }
        string tmp = mpNumberSystemsCoopUI.GetLeftSystemName();
        mpNumberSystemsCoopUI.SetLeftSystemName(mpNumberSystemsCoopUI.GetRightSystemName());
        mpNumberSystemsCoopUI.SetRightSystemName(tmp);
    }


	[ClientRpc]
	private void RpcSendSwap(uint resolver)
	{
		RemoveJoker ();
		if (GetLocalGamePlayer ().netId.Value == resolver)
		{
			this.mpNumberSystemsCoopUI.GetInputFieldLeft ().text = this.mpNumberSystemsCoopUI.GetInputFieldRight().text;
			this.mpNumberSystemsCoopUI.GetInputFieldRight ().text = "";
			this.mpNumberSystemsCoopUI.Unlocked = true;
			mpNumberSystemsCoopUI.GetNumPad ().enabled = true;
			ShowInputDoneGreen ();
			HideInputLocal ();

		}
		else
		{
			mpNumberSystemsCoopUI.Unlocked = false;
			mpNumberSystemsCoopUI.GetSubmitButton ().interactable = false;
			mpNumberSystemsCoopUI.GetNumPad ().enabled = false;
			this.mpNumberSystemsCoopUI.GetInputFieldRight ().text = this.mpNumberSystemsCoopUI.GetInputFieldLeft().text;
			this.mpNumberSystemsCoopUI.GetInputFieldLeft ().text = "";
			ShowInputWaiting ();
			ShowInputLocalDone ();
			HideInputSwap ();
		}
		mpNumberSystemsCoopUI.GetSwapButton ().gameObject.SetActive (false);
		string tmp = mpNumberSystemsCoopUI.GetLeftSystemName ();
		mpNumberSystemsCoopUI.SetLeftSystemName(mpNumberSystemsCoopUI.GetRightSystemName());
		mpNumberSystemsCoopUI.SetRightSystemName(tmp);
	}


	public void UpdateInputContent(string newText)
	{
		player.CmdSendInputChange(newText);
	}

	[Server]
	public void SendInputChange(uint client, string newText)
	{
		RpcUpdatePartnerField (client, newText);
	}

	[ClientRpc]
	private void RpcUpdatePartnerField(uint client, string newText)
	{
		if (client != GetLocalGamePlayer ().netId.Value)
		{
			mpNumberSystemsCoopUI.GetInputFieldLeft ().text = newText;
		} else {
		}
	}
		
	public void ShowInputDone(){
		mpNumberSystemsCoopUI.GetInputDone ().enabled = true;
		mpNumberSystemsCoopUI.GetInputDone ().color = new Color (0F, 0F, 0F);
		mpNumberSystemsCoopUI.GetInputWaiting ().enabled = false;
		mpNumberSystemsCoopUI.GetTextWaiting ().enabled = false;
	}

	public void ShowInputWaiting(){
		mpNumberSystemsCoopUI.GetInputWaiting ().enabled = true;
		mpNumberSystemsCoopUI.GetTextWaiting ().enabled = true;
		mpNumberSystemsCoopUI.GetInputDone ().enabled = false;
	}
		
	public void ShowInputDoneGreen(){
		mpNumberSystemsCoopUI.GetInputDone ().enabled = true;
		mpNumberSystemsCoopUI.GetInputDone ().color = new Color (0.1F, 0.9F, 0F);
		mpNumberSystemsCoopUI.GetInputWaiting ().enabled = false;
		mpNumberSystemsCoopUI.GetTextWaiting ().enabled = false;

	}
	public void HideInputPartner(){
		mpNumberSystemsCoopUI.GetInputDone().enabled = false;
		mpNumberSystemsCoopUI.GetInputWaiting ().enabled = false;
		mpNumberSystemsCoopUI.GetTextWaiting ().enabled = false;
	}

	public void ShowInputLocalDone(){
		mpNumberSystemsCoopUI.GetLocalDone ().enabled = true;
		mpNumberSystemsCoopUI.GetLocalWrong ().enabled = false;

	}

	public void ShowInputLocalWrong(){
		mpNumberSystemsCoopUI.GetLocalDone ().enabled = false;
		mpNumberSystemsCoopUI.GetLocalWrong ().enabled = true;
	}

	public void HideInputLocal(){
		mpNumberSystemsCoopUI.GetLocalDone ().enabled = false;
		mpNumberSystemsCoopUI.GetLocalWrong ().enabled = false;
	}

	public void ShowInputSwap(){
		mpNumberSystemsCoopUI.GetInputSwap ().enabled = true;
	}

	public void HideInputSwap(){
		mpNumberSystemsCoopUI.GetInputSwap().enabled = false;
	}


	[ClientRpc]
	public void RpcShowInputForClientDone(uint playerID){
		if (GetLocalGamePlayer ().netId.Value != playerID)
			ShowInputDone ();
	}

	public void AddJoker()
	{
		if (jokerCards.Count < Global.MAX_SWAPS_COOP)
		{
			Jokercard card = UIElement.Instantiate<Jokercard> (mpNumberSystemsCoopUI.GetJokerHolder ().transform);
			card.transform.Rotate (new Vector3 (0, 0, 1), 10 * (jokerCards.Count - 1));
			jokerCards.AddLast(card);
		}
	}

	public void RemoveJoker()
	{
		Jokercard obj = jokerCards.Last.Value;
		jokerCards.RemoveLast ();
		obj.Hide ();
	}

    [Client]
    protected override UIGameOverview InitGameOverview()
    {
        MPUINumberSystemsCoopOverview gameOverviewUI = UIElement.Instantiate<MPUINumberSystemsCoopOverview>(this.transform);
        gameOverview = gameOverviewUI;
        return gameOverviewUI;
    }
}