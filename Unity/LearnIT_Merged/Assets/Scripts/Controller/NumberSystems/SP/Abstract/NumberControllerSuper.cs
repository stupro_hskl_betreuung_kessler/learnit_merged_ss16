﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public abstract class NumberControllerSuper : SPGameController
{

    protected UINumberSystemsWithHistory numberSystemsUI;
	protected UICustomSlider gameSliderUI;
	protected UIDialog helpDialog;
	private UIGameEndingDialog gameLostDialog;
	private UIGameEndingDialog gameWonDialog;

	protected NumberCreator numberCreator;
	private string leftText;
	private string rightText;

	protected int level = Global.START_LEVEL;
	protected int maxLevel = Global.MAX_LEVEL;

	private int numberSystemLeft = 2;
	private int numberSystemRight = 10;
	private float difficultyScaling = 1.12F;
	private float stretchScaling = 13.05F;
	protected string numberSystemLeftName = Global.BINARY_TEXT;
	protected string numberSystemRightName = Global.DECIMAL_TEXT;

	protected abstract string CreateTitleText();

    protected override void Init()
    {

    }

    protected abstract UICustomSlider InitGameSlider();
    protected abstract string GetGameTitle();
	protected abstract void ResetSlider ();
    protected abstract UIDialog GetHelpDialog();



	protected void ShowWrongInputFeedback(string header, string text, List<UIAnswersEntry> answerUIs, Sprite icon){
        if (null == gameLostDialog) {
			gameLostDialog = UIElement.Instantiate<UIGameEndingDialog> ();
			gameLostDialog.Setup(header, text, answerUIs, icon, FinishGame);
		}
        gameLostDialog.Show ();
	}
		

	protected void ShowGameWonDialog(List<UIAnswersEntry> answerUIs){
		if (null == gameWonDialog) {
			gameWonDialog = UIElement.Instantiate<UIGameEndingDialog> ();
			gameWonDialog.Setup (Global.WIN_TEXT , Global.WELL_DONE_TEXT , answerUIs, Resources.Load<Sprite> (Global.PATH_ICON_WIN), FinishGame);
		}

		gameWonDialog.Show ();
	}
		

	protected abstract void UpdateLevel ();


	protected abstract void SetGameLevel ();

	/**
	 * checks if the submitted result is correct and creates a new number.
	 */
    protected void onSubmitListener()
    {
		string textLeft = numberSystemsUI.GetInputFieldLeft ().text;
        string textRight = numberSystemsUI.GetInputFieldRight().text;
		if(String.IsNullOrEmpty(textLeft) || string.IsNullOrEmpty(textRight)){
			return;
		}

        numberSystemsUI.AddAnswer(leftText, textRight, rightText);


        if (textLeft == leftText && textRight == rightText)
        {
            OnCorrectGuess();
        }
        else
        {
            OnFalseGuess();
        }
        numberSystemsUI.GetNumPad().ClearDigits();
        createNumber();
    }

    protected abstract void OnCorrectGuess();
    protected abstract void OnFalseGuess();

    protected virtual void ShowNextRoundFeedback(string textToShow)
    {
        StartCoroutine(numberSystemsUI.GetTextFlasher().Flash(Global.COLOR_FLASH_GREEN, textToShow));
        StartCoroutine(numberSystemsUI.GetScreenFlasher().Flash(Global.COLOR_FLASH_GREEN));
    }

    protected virtual void ShowWrongNumberFeedback()
    {
        StartCoroutine(numberSystemsUI.GetScreenFlasher().Flash(Global.COLOR_FLASH_RED));

    }

    protected void createNumber()
    {     
		string[] result = numberCreator.createNumbers (getDigits());
		this.leftText = result [0];
		this.rightText = result [1];

		numberSystemsUI.GetInputFieldLeft().text = leftText;
    }

    protected override IEnumerator LoadGame()
    {
        //load number systems from game mode
        GameMode lastChosenMode = GameMenuController.GetLastChosenMode();
        GameModeNumberSystem mode = lastChosenMode is GameModeNumberSystem ? (GameModeNumberSystem)lastChosenMode : null;
        if (null == mode)
            yield break;

        GameModeNumberSystem.NumberSystemData numberSystem = mode.GetNumberSystem();
        numberSystemLeft = numberSystem.GetNumberSystemLeft();
        numberSystemRight = numberSystem.GetNumberSystemRight();
        difficultyScaling = numberSystem.GetDifficultyScaling();
        stretchScaling = numberSystem.GetStretchScaling();
        numberSystemLeftName = numberSystem.GetNumberSystemLeftName();
        numberSystemRightName = numberSystem.GetNumberSystemRightName();

        yield break;
    }

    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION BEGIN //////////////////////////////////
    public override void OnHelp()
    {
        helpDialog.Show();
    }

    public override bool HasHelpOption()
    {
        return false;
    }
    ///////////////////////// INTERFACE IPAUSEHANDLER IMPLEMENTATION END //////////////////////////////////

    public abstract void OnSliderFinished();

    protected override void OnGameFinished()
    {
		
    }

    protected override void OnGameLoaded()
    {
        numberCreator = this.GetNumberCreator();
        numberSystemsUI = UIElement.Instantiate<UINumberSystemsWithHistory>(this.transform);
        numberSystemsUI.CreateNumPad(numberCreator);
        numberSystemsUI.GetSubmitButton().onClick.AddListener(onSubmitListener);
        createNumber();

        gameSliderUI = InitGameSlider();
        gameSliderUI.OnFinished += OnSliderFinished;

        numberSystemsUI.SetGameSlider(gameSliderUI);
        numberSystemsUI.SetTitleText(GetGameTitle());
        numberSystemsUI.GetNumPad().AddOnSubmitAction(onSubmitListener);
        numberSystemsUI.AddPauseButtonAction(PauseGame);
        numberSystemsUI.SetLeftNumberSystemName(numberSystemLeftName);
        numberSystemsUI.SetRightNumberSystemName(numberSystemRightName);
        numberSystemsUI.SetTitleText(CreateTitleText());
        UpdateLevel();

        numberSystemsUI.SetGameSlider(gameSliderUI);
        helpDialog = GetHelpDialog();
    }

	protected virtual NumberCreator GetNumberCreator ()
	{
		return new NumberCreator (numberSystemLeft, numberSystemRight);
	}

	/**
	 * Calculates the amount of digits using the difficultyScaling, the strechScaling and the level.
	 */
	protected int getDigits ()
	{
		return (int)(Mathf.Log(level + stretchScaling - 1)/Mathf.Log(difficultyScaling) + 1 - Mathf.Log(stretchScaling)/Mathf.Log(difficultyScaling));
	}
}