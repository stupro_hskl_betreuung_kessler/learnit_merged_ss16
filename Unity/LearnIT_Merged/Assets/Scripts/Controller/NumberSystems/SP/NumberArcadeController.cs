﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using System;

public class NumberArcadeController : NumberControllerSuper {

    
	private UICountSlider slider;
	private new UIInfoDialog helpDialog;
	protected int maxTries  = Global.MAX_TRIES;

    protected override UICustomSlider InitGameSlider()
    {
		slider = UIElement.Instantiate<UICountSlider>();
		slider.Reset (maxTries);
		slider.SetAppendingText (" / " + maxTries);
		slider.SetIsCountingDown (true);
		return slider;
    }

	protected override void ResetSlider(){
		slider.Reset (maxTries);
	}

    public override void OnSliderFinished()
    {
        Debug.LogError("sliderfinish() - " + numberSystemsUI.GetAnswers().Count);
		ShowWrongInputFeedback (Global.LOST_TEXT , Global.END_TEXT_ARCADE , numberSystemsUI.GetAnswers(), Resources.Load<Sprite> (Global.PATH_ICON_ERROR));

    }

	protected override void UpdateLevel()
	{
		if (level > maxLevel) {
			numberSystemsUI.GetInputFieldLeft().text = "";
		}
		else {
			SetGameLevel ();
			ResetSlider ();
		}
	}

    protected override void OnCorrectGuess()
    {
		if (level == maxLevel)
			ShowGameWonDialog (numberSystemsUI.GetAnswers());
		else {
			level++;
			UpdateLevel ();
			ShowNextRoundFeedback (Global.LEVEL_TEXT + " " + level);
		}
    }

    protected override void OnFalseGuess()
    {
		ShowWrongNumberFeedback ();
		slider.Decrement ();
	
    }

    protected override string GetGameTitle()
    {
		return Global.GAME_TITLE_ARCADE;
    }

    
    protected override UIDialog GetHelpDialog()
    {
        if (null == helpDialog)
        {
            helpDialog = UIElement.Instantiate<UIInfoDialog>(transform);
            helpDialog.Setup(
                Global.STRING_HELP,
                //TODO
                "????",
                Resources.Load<Sprite>(Global.PATH_ICON_QUESTIONMARK)
            );
        }
        return helpDialog;
    }

	protected override void SetGameLevel ()
	{
		numberSystemsUI.SetGameLevel (level, maxLevel);
	}

	public override void OnRestartGame ()
	{
		base.OnRestartGame ();
		this.level = 1;
		this.UpdateLevel ();
		this.slider.Reset (maxTries);
		base.createNumber ();
		numberSystemsUI.GetInputFieldRight ().text = "";
	}

	protected override string CreateTitleText()
	{
		return numberSystemLeftName + " -> " + numberSystemRightName + " " + Global.ARCADE_TEXT;
	}
}