﻿using UnityEngine;
using Utils;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class SceneLoader : MonoBehaviour{

    //Scenes
    public static readonly string SCENE_GAME_SELECTION = "Main Scene";
    public static readonly string SCENE_LOBBY = "Lobby Scene";
    public static readonly string SCENE_GAME_MENU = "GameMenu Scene";
    public static readonly string SCENE_THEORY = "Theory Scene";
    public static readonly string SCENE_QUIZ = "Quiz Scene";
    public static readonly string SCENE_QUIZ_MP = "MP Quiz Scene";
    public static readonly string SCENE_IT_NUMBER_ARCADE = "ITNumberArcade Scene";
    public static readonly string SCENE_IT_NUMBER_SURVIVAL = "ITNumberSurvival Scene";
    public static readonly string SCENE_IT_NUMBER_SURVIVAL_MP = "MP ITNumberSurvival Scene";
    public static readonly string SCENE_IT_NUMBER_BLIND_SURVIVAL_MP = "MP Blind ITNumberSurvival Scene";
    public static readonly string SCENE_CP_GC_ARCADE = "CPGCArcade Scene";
    public static readonly string SCENE_CP_GC_SURVIVAL = "CPGCSurvival Scene";
    public static readonly string SCENE_CP_GC_MP = "MP CPGC Scene";
    public static readonly string SCENE_CP_GC_COOP_MP = "MP COOP CPGC Scene";

    private static string previousScene = null;
    private static string currentScene = SCENE_GAME_SELECTION;

    void Start(){
        //Init required singletons
        GameManager.GetInstance();
    }

    public void LoadGameSelectionScene()
    {
        LoadScene(SCENE_GAME_SELECTION, false);
    }

    public void LoadGameMenuScene(bool showTransition = true)
    {
        LoadScene(SCENE_GAME_MENU, showTransition);
    }

    public void LoadPreviousScene(bool showTransition = true)
    {
        if (null == previousScene)
            return;

        LoadScene(previousScene, showTransition);
    }

    public void LoadScene(GameMode mode, bool showTransition = true)
    {
        if (null == mode)
            return;

        LoadScene(mode.GetGameSceneName(), showTransition);
    }

    private void LoadScene(string name, bool showTransition)
    {
        //Debug.Log("Loading Scene: " + name);
        previousScene = currentScene;
        currentScene = name;

        if (showTransition)
            UISceneFader.GetInstance().TransitionToScene(name);
        else
            Application.LoadLevel(name);
    }

    public void QuitApplication()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else 
        Application.Quit();
#endif
    }
}
