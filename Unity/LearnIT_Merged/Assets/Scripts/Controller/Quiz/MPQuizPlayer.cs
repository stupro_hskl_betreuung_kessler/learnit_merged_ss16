﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.Collections;
using System.IO;

public class MPQuizPlayer : MPGamePlayer {

    public static readonly string PREFAB_PATH = Global.PATH_PREFABS_DIR + "Network" + Path.AltDirectorySeparatorChar + "MPQuizPlayer";

    private UIQuizPlayer quizPlayerUI;
    private MPQuizController quizController;

    [SyncVar(hook = "SetNumOfQuestions")]
    private int numOfQuestions;

    [SyncVar(hook = "SetNumOfCorrectAnswers")]
    private int numOfCorrectAnswers;

    [SyncVar(hook = "SetUsedTimeToSolve")]
    private float usedTimeToSolve;

    [SyncVar]
    private bool receivedAllQuestions = false;

    [SyncVar]
    private bool answeredAllQuestions = false;

    protected override void InitClientSide()
    {
        SetNumOfQuestions(numOfQuestions);
        SetNumOfCorrectAnswers(numOfCorrectAnswers);
    }

    protected override UIMPGamePlayer InitPlayerUI()
    {
        quizPlayerUI = UIElement.Instantiate<UIQuizPlayer>(this.transform);
        return quizPlayerUI;
    }

    public void SetNumOfQuestions(int num)
    {
        this.numOfQuestions = num;

        if (null != quizPlayerUI)
            quizPlayerUI.SetTotalNumberOfQuestions(numOfQuestions);
    }

    public void SetNumOfCorrectAnswers(int num)
    {
        this.numOfCorrectAnswers = num;
        if (null != quizPlayerUI)
            quizPlayerUI.SetNumberOfCorrectAnswers(num);
    }

    public void SetUsedTimeToSolve(float usedTime)
    {
        this.usedTimeToSolve = usedTime;
        if (null != quizPlayerUI)
            quizPlayerUI.SetUsedTimeToSolveText(usedTimeToSolve);
    }

    public void IncreaseUsedTimeToSolve(float timeToAdd)
    {
        SetUsedTimeToSolve(usedTimeToSolve + timeToAdd);
    }

    public int GetNumOfQuestions()
    {
        return numOfQuestions;
    }

    public int GetNumOfCorrectAnswers()
    {
        return numOfCorrectAnswers;
    }

    public bool ReceivedAllQuestions()
    {
        return receivedAllQuestions;
    }

    public bool AnsweredAllQuestions()
    {
        return answeredAllQuestions;
    }

    public void IncrementNumOfCorrectAnswers()
    {
        SetNumOfCorrectAnswers(numOfCorrectAnswers + 1);
    }

    [Command]
    public void CmdAnswer(int questionIndex, int answerIndex, float remainingTime)
    {
        if (null == quizController)
            quizController = (MPQuizController)gameController;

        quizController.OnAnswerSelected(questionIndex, answerIndex, this, remainingTime);
    }

    [Command]
    public void CmdReceivedAllQuestions()
    {
        if (null == quizController)
            quizController = (MPQuizController)gameController;

        this.receivedAllQuestions = true;
        quizController.OnClientReceivedAllQuestions(this);
    }

    [Command]
    public void CmdAnsweredAllQuestions()
    {
        if (null == quizController)
            quizController = (MPQuizController)gameController;

        this.answeredAllQuestions = true;
        quizController.OnClientAnsweredAllQuestions(this);
    }
}
