﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using System.IO;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;

[RequireComponent(typeof(NetworkTransmitter))]
public class MPQuizController : MPGameController
{
    //using a higher countdown here than in super class, because the overview holds more information to be analyzed by the student
    private const int COUNTDOWN_TO_RETURN_TO_LOBBY = 25; 

    //game Specific UI
    private UIMPQuiz quizUI;
    private UIQuizOverview quizOverviewUI;
    private UIImageFlasher screenFlasherUI;
    private UITimeSlider gameSliderUI;

    //Game Specific Logic
    private QuizQuestion[] serverQuestions;
    private QuizQuestion[] clientQuestions;
    private int clientQuestionIndex;
    private MPQuizPlayer localQuizPlayer;

    //for question transmission
    private NetworkTransmitter networkTransmitter;
    private int totalReceivedQuestionBytes = 0;
    private int totalExpectedQuestionBytes = 0;

    //////////////////////////////////////////////////////Shared Logic Begin (Client and Server)
    protected override void InitBothSides()
    {
        int reliableSequencedChannel = -1;
        for (int i = 0; i < lobby.channels.Count; i++)
            if (lobby.channels[i] == QosType.ReliableSequenced)
                reliableSequencedChannel = i;

        if (reliableSequencedChannel != LobbyController.CHANNEL_RELIABLE_SEQUENCED)
        {
            Debug.LogError(LOG_PREFIX + "Can not send QuizQuestionData to client. Qos Channel 'ReliableSequenced' not found at LobbyController.CHANNEL_RELIABLE_SEQUENCED="
                + LobbyController.CHANNEL_RELIABLE_SEQUENCED + "... reliableSequencedChannel=" + reliableSequencedChannel);
        }

        networkTransmitter = GetComponent<NetworkTransmitter>();
    }

    protected override void InitServerSide()
    {
        if (null == networkTransmitter)
        {
            Debug.LogError(LOG_PREFIX + "Did not find NetworkTransmitter. Can not transmit QuestionData. Returning to lobby.");
            lobby.InvokeReturnToLobby();
            return;
        }
    }

    protected override void InitClientSide()
    {
        //init UI for clients
        gameSliderUI = UIElement.Instantiate<UITimeSlider>();
        gameSliderUI.Reset(10);
        gameSliderUI.OnFinished += () =>
        {
            TryToAnswer(-1);
        };
        gameSliderUI.Pause();
        
        quizUI = UIElement.Instantiate<UIMPQuiz>(this.transform);
        quizUI.SetGameSlider(gameSliderUI);
        quizUI.SetTitleText(lobby.GetGameModeToPlay().GetName());
        quizUI.SetPauseButtonImage(Resources.Load<Sprite>(Global.PATH_ICON_EXIT));
        quizUI.AddPauseButtonAction(lobby.InvokeLeaveLobbyButton);
        quizUI.Hide();

        screenFlasherUI = UIElement.Instantiate<UIImageFlasher>(this.transform);
        screenFlasherUI.Hide();

        loadingGameIndicator.SetShowProgressBar(true);
        loadingGameIndicator.SetIndicationText(Global.STRING_INDICATE_LOADING_QUIZ);

        networkTransmitter.OnDataCompletelyReceived += OnReceiveQuestion;
        networkTransmitter.OnDataFragmentReceived += OnReceiveQuestionFragment;
    }

    public MPQuizPlayer GetLocalQuizPlayer()
    {
        if (localQuizPlayer == null)
            localQuizPlayer = (MPQuizPlayer)GetLocalGamePlayer();
        return localQuizPlayer;
    }
    //////////////////////////////////////////////////////Shared Logic End (Client and Server)

    //////////////////////////////////////////////////////Server Logic Begin

    [Server]
    protected override void OnAllPlayersReadyToBeginMatch()
    {
        Debug.Log(LOG_PREFIX + "OnServerAllPlayersReadyToBeginMatch");

        //players are ready. generate quiz and send questionts to the clients
        GameModeQuiz quizMode = lobby.GetGameModeToPlay() is GameModeQuiz ? (GameModeQuiz)lobby.GetGameModeToPlay() : null; 
        Quiz quiz = null;
        if (null != quizMode)
            quiz = quizMode.GetQuiz();
        if (null != quiz)
            serverQuestions = quiz.GetQuestions();
        if (null == serverQuestions || serverQuestions.Length < 1)
        {
            Debug.LogWarning(LOG_PREFIX + "Could not load Quiz Data. Returning to lobby.");
            UIToastFlasher.CreateAndDisplayToast(this, Color.white, Global.COLOR_FLASH_RED, "Quiz konnte nicht geladen werden. Ist die Quiz XML korrupt?");
            lobby.InvokeReturnToLobby();
            return;
        }

        StartCoroutine(DistributeQuizQuestions());
    }

    [Server]
    private IEnumerator DistributeQuizQuestions()
    {
        Debug.Log(LOG_PREFIX + "Preparing data of QuizQuestions to distribute to clients ...");

        byte[][] questionDataToSend = new byte[serverQuestions.Length][];
        int[] sizes = new int[serverQuestions.Length];
        for (int i = 0; i < serverQuestions.Length; i++)
        {
            byte[] imageBytes = QuizLoader.LoadQuizImageBytesFromFile(serverQuestions[i].GetQuestionImageName());
            serverQuestions[i].SetQuestionImageBytes(imageBytes);
            questionDataToSend[i] = serverQuestions[i].SerializeToByteArray();
            sizes[i] = questionDataToSend[i].Length;
        }

        //tell clients how many questions they have to expect from server, and how big each question is
        RpcPrepareToReceiveQuizQuestions(sizes);
        foreach (MPGamePlayer gp in gamePlayers.Values)
        {
            ((MPQuizPlayer)gp).SetNumOfQuestions(serverQuestions.Length);
        }

        Debug.Log(LOG_PREFIX + "Starting to distribute data of QuizQuestions to clients ...");

        yield return null;

        //share generated questions with all clients
        for (int i = 0; i < questionDataToSend.Length; i++)
        {         
            yield return  StartCoroutine(networkTransmitter.SendBytesToClientsRoutine(i, questionDataToSend[i]));
        }
    }

    [Server]
    public void OnClientReceivedAllQuestions(MPQuizPlayer quizPlayer)
    {
        Debug.Log(LOG_PREFIX + "OnClientReceivedAllQuestions - " + quizPlayer);

        //check if all clients received all questions
        bool allReceivedEverything = true;
        foreach (MPGamePlayer gp in gamePlayers.Values)
        {
            MPQuizPlayer qp = (MPQuizPlayer)gp;
            if (!qp.ReceivedAllQuestions())
            {
                allReceivedEverything = false;
                break;
            }
        }

        if (allReceivedEverything)
            OnAllClientsReceivedAllQuestions();
    }

    [Server]
    public void OnAllClientsReceivedAllQuestions()
    {
        Debug.Log(LOG_PREFIX + "OnClientReceivedAllQuestions");
        StartMatch();
    }

    [Server]
    public void OnClientAnsweredAllQuestions(MPQuizPlayer quizPlayer)
    {
        Debug.Log(LOG_PREFIX + "OnClientAnsweredAllQuestions - " + quizPlayer);

        //check if all clients answered all questions
        bool allAnsweredEverything = true;
        foreach (MPGamePlayer gp in gamePlayers.Values)
        {
            MPQuizPlayer qp = (MPQuizPlayer)gp;
            if (!qp.AnsweredAllQuestions())
            {
                allAnsweredEverything = false;
                break;
            }
        }

        if (allAnsweredEverything)
            OnAllClientsAnsweredAllQuestions();
    }

    [Server]
    public void OnAllClientsAnsweredAllQuestions()
    {
        Debug.Log(LOG_PREFIX + "OnAllClientsAnsweredAllQuestions");
        FinishMatch();
    }

    [Server]
    public void OnAnswerSelected(int questionIndex, int selectedAnswerIndex, MPQuizPlayer answeringPlayer, float remainingTime)
    {
        QuizQuestion currentQuestion = serverQuestions[questionIndex];
        currentQuestion.Reset();

        currentQuestion.GiveAnswer(selectedAnswerIndex);
        QuizQuestion.AnswerState answerState = currentQuestion.GetAnswerState();

        switch (answerState)
        {
            case QuizQuestion.AnswerState.CORRECT_ANSWERED:
                RpcOnCorrectAnswer(questionIndex, selectedAnswerIndex, answeringPlayer.netId.Value);
                break;
            case QuizQuestion.AnswerState.WRONG_ANSWERED:
                RpcOnWrongAnswer(questionIndex, selectedAnswerIndex, answeringPlayer.netId.Value);
                break;
            default:
                break;
        }

        //update used time to solve of player
        answeringPlayer.IncreaseUsedTimeToSolve(currentQuestion.GetSolveDuration() - remainingTime);
    }

    protected override int GetCountdownForLobby()
    {
        return COUNTDOWN_TO_RETURN_TO_LOBBY;
    }
    //////////////////////////////////////////////////////Server Logic End

    //////////////////////////////////////////////////////Client Logic Begin
    [Client]
    private void TryToAnswer(int selectedAnswerIndex)
    {
        if (clientQuestions[clientQuestionIndex].IsAnswered())
            return;

        clientQuestions[clientQuestionIndex].GiveAnswer(selectedAnswerIndex);
        GetLocalQuizPlayer().CmdAnswer(clientQuestionIndex, selectedAnswerIndex, gameSliderUI.GetCurrentValue());
    }

    [ClientRpc]
    private void RpcPrepareToReceiveQuizQuestions(int[] questionSizes)
    {
        foreach (int size in questionSizes)
            totalExpectedQuestionBytes += size;

        clientQuestions = new QuizQuestion[questionSizes.Length];

        Debug.Log(LOG_PREFIX + "RpcInitQuizQuestions size=" + questionSizes.Length + "totalsize=" + totalExpectedQuestionBytes);
    }

    [Client]
    private void OnReceiveQuestion(int questionIndex, byte[] questionBytes)
    {
        //already received this question?
        if (null != clientQuestions[questionIndex])
            return;

        Debug.Log(LOG_PREFIX + "Completely Received Question at index=" + questionIndex);
        clientQuestions[questionIndex] = ObjectSerializationExtension.Deserialize<QuizQuestion>(questionBytes);

        //received first question? --> setup ui
        if (questionIndex == 0)
            SetupQuizUI();

        //check if we received all questions
        bool receivedAll = true;
        for (int i = 0; i < clientQuestions.Length; i++)
        {
            if (null == clientQuestions[i])
            {
                receivedAll = false;
                break;
            }
        }

        //notify server, when this client received all questions -> tell him we are ready to play
        if (receivedAll)
        {
            quizOverviewUI.InitQuestionResults(clientQuestions);
            loadingGameIndicator.SetIndicationText(Global.STRING_INDICATE_WAITING_FOR_PLAYERS);
            loadingGameIndicator.SetShowProgressBar(false);
            GetLocalQuizPlayer().CmdReceivedAllQuestions();
        }
    }

    [Client]
    private void OnReceiveQuestionFragment(int questionIndex, byte[] questionFragment)
    {
        UpdateReceivingProgress(questionFragment.Length);
    }

    [Client]
    private void UpdateReceivingProgress(int receivedBytes)
    {
        totalReceivedQuestionBytes += receivedBytes;
        loadingGameIndicator.UpdateProgress(totalReceivedQuestionBytes / (float)totalExpectedQuestionBytes);
    }

    [Client]
    private void SetupQuizUI()
    {
        quizUI.Setup(clientQuestions[0], clientQuestions.Length, TryToAnswer);
        gameSliderUI.Reset(clientQuestions[0].GetSolveDuration());

        //the MPQuizUI currently supports only 2 players.
        //If there are more than one remote player, their visuals will be overriden
        foreach (MPGamePlayer gp in gamePlayers.Values)
        {
            if (gp.isLocalPlayer)
                quizUI.SetupLocalPlayerVisuals(gp);
            else
                quizUI.SetupRemotePlayerVisuals(gp);
        }
    }

    [ClientRpc]
    private void RpcOnCorrectAnswer(int questionIndex, int selectedAnswerIndex, uint answeringPlayerId)
    {
        StartCoroutine(OnCorrectAnswer(questionIndex, selectedAnswerIndex, answeringPlayerId));
    }

    [ClientRpc]
    private void RpcOnWrongAnswer(int questionIndex, int selectedAnswerIndex, uint answeringPlayerId)
    {
        StartCoroutine(OnWrongAnswer(questionIndex, selectedAnswerIndex, answeringPlayerId));
    }

    [Client]
    private IEnumerator OnCorrectAnswer(int questionIndex, int selectedAnswerIndex, uint answeringPlayerId)
    {
        MPGamePlayer answeringPlayer = GetGamePlayer(answeringPlayerId);
        
        //remote player answered a question? only update result, don't intervene our game
        if (!answeringPlayer.isLocalPlayer)
        {
            quizUI.UpdateOpponentProgressMarker(questionIndex, true);
            quizOverviewUI.UpdateQuestionResult(questionIndex, QuizQuestion.AnswerState.CORRECT_ANSWERED, (UIQuizPlayer)answeringPlayer.GetUI());
            yield break;
        }


        //show and wait for local feedback, then advance to next question
        gameSliderUI.Pause();
        quizUI.UpdateProgressMarker(questionIndex, true);
        quizOverviewUI.UpdateQuestionResult(questionIndex, QuizQuestion.AnswerState.CORRECT_ANSWERED, (UIQuizPlayer)answeringPlayer.GetUI());
        yield return StartCoroutine(ShowCorrectFeedback());

        NextQuestion();
    }

    [Client]
    private IEnumerator OnWrongAnswer(int questionIndex, int selectedAnswerIndex, uint answeringPlayerId)
    {
        MPGamePlayer answeringPlayer = GetGamePlayer(answeringPlayerId);

        //remote player answered a question? only update result, don't intervene our game
        if (!answeringPlayer.isLocalPlayer)
        {
            quizUI.UpdateOpponentProgressMarker(questionIndex, false);
            quizOverviewUI.UpdateQuestionResult(questionIndex, QuizQuestion.AnswerState.WRONG_ANSWERED, (UIQuizPlayer)answeringPlayer.GetUI());
            yield break;
        }


        //show and wait for local feedback, then advance to next question
        gameSliderUI.Pause();
        quizUI.UpdateProgressMarker(questionIndex, false);
        quizOverviewUI.UpdateQuestionResult(questionIndex, QuizQuestion.AnswerState.WRONG_ANSWERED, (UIQuizPlayer)answeringPlayer.GetUI());
        yield return StartCoroutine(ShowWrongFeedback());

        NextQuestion();
    }

    [Client]
    private IEnumerator ShowCorrectFeedback()
    {
        audioPlayer.PlayCorrectSound();
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_GREEN));
        yield return StartCoroutine(quizUI.HighlightAnswerButton(clientQuestions[clientQuestionIndex]));
    }

    [Client]
    private IEnumerator ShowWrongFeedback()
    {
        audioPlayer.PlayWrongSound();
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_RED));
        yield return StartCoroutine(quizUI.HighlightAnswerButton(clientQuestions[clientQuestionIndex]));
    }

    [Client]
    private void NextQuestion()
    {
        clientQuestionIndex++;
        if (clientQuestionIndex >= clientQuestions.Length)
        {
            OnQuizFinished();
            return;
        }

        quizUI.SetQuestion(clientQuestions[clientQuestionIndex]);
        quizUI.SetProgressText(clientQuestionIndex, clientQuestions.Length);
        gameSliderUI.Reset(clientQuestions[clientQuestionIndex].GetSolveDuration());
        gameSliderUI.Continue();
    }

    [Client]
    private void OnQuizFinished()
    {
        gameSliderUI.Pause();
        quizOverviewUI.Show();     

        //reset answers, in case another round is played
        foreach (QuizQuestion qq in clientQuestions)
            qq.Reset();

        //notify server that client finished quiz
        GetLocalQuizPlayer().CmdAnsweredAllQuestions();
    }

    [Client]
    protected override UIGameOverview InitGameOverview()
    {
        quizOverviewUI = UIElement.Instantiate<UIQuizOverview>(this.transform);
        return quizOverviewUI;
    }

    [Client]
    protected override void OnMatchStarted()
    {
        quizUI.Show();
        screenFlasherUI.Show();
        gameSliderUI.Continue();
        audioPlayer.PlayQuizMusic();
    }

    [Client]
    protected override void OnMatchFinished()
    {
        screenFlasherUI.Hide();
        gameSliderUI.Pause();
        audioPlayer.StopBgMusic();
    }

    protected override void ShowGameOverviewOnTab()
    {
        if (null!=GetLocalQuizPlayer() && GetLocalQuizPlayer().AnsweredAllQuestions())
            return;

        base.ShowGameOverviewOnTab();
    }
    //////////////////////////////////////////////////////Client Logic End
}
