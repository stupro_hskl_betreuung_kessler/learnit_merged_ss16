﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.Collections;
using System.IO;

public class MPGCPlayer : MPGamePlayer {

    public static readonly string PREFAB_PATH = Global.PATH_PREFABS_DIR + "Network" + Path.AltDirectorySeparatorChar + "MPGCPlayer";

    private UIGCPlayer gcPlayerUI;
    private MPGCController gcController;

    [SyncVar(hook = "SetNumOfGraphs")]
    private int numOfGraphs;

    [SyncVar(hook = "SetNumOfCorrectGraphs")]
    private int numOfCorrectGraphs;

    [SyncVar(hook = "SetNumOfColorizations")]
    private int numOfColorizations;

    [SyncVar(hook = "SetNumOfMistakes")]
    private int numOfMistakes;

    [SyncVar(hook = "SetUsedTimeToSolve")]
    private float usedTimeToSolve;
    

    [SyncVar]
    private bool completedLoadingGraphs = false;

    [SyncVar]
    private bool completedAllGraphs = false;

    protected override void InitClientSide()
    {
        SetNumOfGraphs(numOfGraphs);
        SetNumOfCorrectGraphs(numOfCorrectGraphs);
    }

    protected override UIMPGamePlayer InitPlayerUI()
    {
        gcPlayerUI = UIElement.Instantiate<UIGCPlayer>(this.transform);
        return gcPlayerUI;
    }

    public void SetNumOfGraphs(int num)
    {
        this.numOfGraphs = num;

        if (null != gcPlayerUI)
            gcPlayerUI.SetTotalNumberOfGraphs(numOfGraphs);
    }

    public void SetNumOfCorrectGraphs(int num)
    {
        this.numOfCorrectGraphs = num;
        if (null != gcPlayerUI)
            gcPlayerUI.SetNumOfCorrectGraphs(numOfCorrectGraphs);
    }

    public void SetNumOfColorizations(int num)
    {
        this.numOfColorizations = num;
        if (null != gcPlayerUI)
            gcPlayerUI.SetNumOfColorizationsText(numOfColorizations);
    }

    public void SetNumOfMistakes(int num)
    {
        this.numOfMistakes = num;
        if (null != gcPlayerUI)
            gcPlayerUI.SetNumOfMistakesText(numOfMistakes);
    }

    public void SetUsedTimeToSolve(float usedTime)
    {
        this.usedTimeToSolve = usedTime;
        if (null != gcPlayerUI)
            gcPlayerUI.SetUsedTimeToSolveText(usedTimeToSolve);
    }

    public void IncreaseUsedTimeToSolve(float timeToAdd)
    {
        SetUsedTimeToSolve(usedTimeToSolve + timeToAdd);
    }

    public bool CompletedLoadingGraphs()
    {
        return completedLoadingGraphs;
    }

    public bool CompletedAllGraphs()
    {
        return completedAllGraphs;
    }

    public void IncrementNumOfCorrectGraphs()
    {
        SetNumOfCorrectGraphs(numOfCorrectGraphs + 1);
    }

    [Command]
    public void CmdCompletedGraph(int graphIndex, bool success, float remainingTime)
    {
        if (null == gcController)
            gcController = (MPGCController)gameController;

        gcController.OnClientCompletedGraph(graphIndex, success, this, remainingTime);
    }

    [Command]
    public void CmdCompletedLoadingGraphs()
    {
        if (null == gcController)
            gcController = (MPGCController)gameController;

        this.completedLoadingGraphs = true;
        gcController.OnClientCompletedLoadingGraphs(this);
    }

    [Command]
    public void CmdCompletedAllGraphs()
    {
        if (null == gcController)
            gcController = (MPGCController)gameController;

        this.completedAllGraphs = true;
        gcController.OnClientCompletedAllGraphs(this);
    }

    [Command]
    public void CmdColorizedNode()
    {
        this.numOfColorizations++;
    }

    [Command]
    public void CmdMadeMistake()
    {
        this.numOfMistakes++;
    }
}
