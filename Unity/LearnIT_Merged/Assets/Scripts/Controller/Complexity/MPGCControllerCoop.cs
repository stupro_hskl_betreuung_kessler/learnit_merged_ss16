﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
using Utils;
using UnityEngine.Networking;

public class MPGCControllerCoop : MPGameController
{
    //using a higher countdown here than in super class, because the overview holds more information to be analyzed by the student
    private const int COUNTDOWN_TO_RETURN_TO_LOBBY = 25;
    private const int NUM_OF_GRAPHS_PER_MATCH = 3;

    private GCLoader gcLoader;

    private List<GCGraph> graphs;
    private GCGraph currentGraph;
    private Color currentColor = Color.white;
    private bool nodesAreFlickering = false; //true while the flicker animation is playing to prevent further click processing
    private bool advancingToNextGraph = false; //true while setting next graph to prevent further click processing
    private MPGCPlayerCoop localGCPlayer;

    private Texture2D paintBrushTexture;
    private UIMPGraphColoringCoop gcUI;
    private UIGCOverview gcOverviewUI;
    private UIImageDialog solutionDialog;
    private UIImageFlasher screenFlasherUI;
    private UITimeSlider gameSlider;

    //////////////////////////////////////////////////////Shared Logic Begin (Client and Server)

    protected override void InitBothSides()
    {
        gameSlider = UIElement.Instantiate<UITimeSlider>();
        gameSlider.Pause();
    }

    protected override void InitServerSide()
    {
        //server controlls timer
        gameSlider.OnFinished += () =>
        {
            OnClientsFailedGraph();
        };
    }

    protected override void InitClientSide()
    {
        gcLoader = gameObject.AddComponent<GCLoader>();

        loadingGameIndicator.SetShowProgressBar(true);
        loadingGameIndicator.SetIndicationText(Global.STRING_INDICATE_LOADING_GAME);

        gcUI = UIElement.Instantiate<UIMPGraphColoringCoop>();
        gcUI.SetOnGraphImageClickListener(OnGraphImageClick);
        gcUI.SetOnColorButtonClickListener(OnColorButtonClick);
        gcUI.SetOnEraseButtonClickListener(OnEraseButtonClick);
        gcUI.SetOnClearButtonClickListener(OnClearButtonClick);
        gcUI.SetTitle(lobby.GetGameModeToPlay().GetName());
        gcUI.SetPauseButtonImage(Resources.Load<Sprite>(Global.PATH_ICON_EXIT));
        gcUI.AddPauseButtonAction(lobby.InvokeLeaveLobbyButton);
        gcUI.Hide();
        gcUI.SetGameSlider(gameSlider);

        solutionDialog = UIElement.Instantiate<UIImageDialog>();
        solutionDialog.SetDialogTitle(Global.STRING_SOLUTION);
        solutionDialog.Hide();

        //feedback
        audioPlayer = gameObject.AddComponent<AudioPlayer>();
        screenFlasherUI = UIElement.Instantiate<UIImageFlasher>(this.transform);
    }

    public MPGCPlayerCoop GetLocalGCPlayer()
    {
        if (localGCPlayer == null)
            localGCPlayer = (MPGCPlayerCoop)GetLocalGamePlayer();
        return localGCPlayer;
    }

    float lastTimeSync = 0;
    public override void Update()
    {
        base.Update();
        if (!isServer)
            return;

        //sync timer every 5 seconds
        float diffToLastSync = Time.time - lastTimeSync;
        if (diffToLastSync < 5)
            return;
        RpcSyncTimer(gameSlider.GetCurrentValue());
        lastTimeSync = Time.time;
    }

    //////////////////////////////////////////////////////Shared Logic End (Client and Server)

    //////////////////////////////////////////////////////Server Logic Begin

    [Server]
    protected override void OnAllPlayersReadyToBeginMatch()
    {
        Debug.Log(LOG_PREFIX + "OnServerAllPlayersReadyToBeginMatch");

        //players are ready. determine graphs and tell clients to load them
        GameModeCP gameMode = (GameModeCP)lobby.GetGameModeToPlay();
        List<GCGraphData> graphsToLoad = gameMode.GetGraphData(NUM_OF_GRAPHS_PER_MATCH);
        int[] graphDataIds = new int[graphsToLoad.Count];
        for (int i = 0; i < graphDataIds.Length; i++)
            graphDataIds[i] = graphsToLoad[i].GetId();

        foreach (MPGamePlayer gp in gamePlayers.Values)
        {
            MPGCPlayerCoop p = (MPGCPlayerCoop)gp;
            if (!p.CompletedAllGraphs())
            {
                p.SetNumOfGraphs(graphDataIds.Length);
            }
        }

        RpcReceiveGraphSelection(graphDataIds);
    }

    [Server]
    public void OnClientCompletedLoadingGraphs(MPGCPlayerCoop player)
    {
        Debug.Log(LOG_PREFIX + "OnClientCompletedLoadingGraphs - " + player);

        //check if all clients loaded all graphs
        bool allLoadedEverything = true;
        foreach (MPGamePlayer gp in gamePlayers.Values)
        {
            MPGCPlayerCoop qp = (MPGCPlayerCoop)gp;
            if (!qp.CompletedLoadingGraphs())
            {
                allLoadedEverything = false;
                break;
            }
        }

        if (allLoadedEverything)
            OnAllClientsCompletedLoadingGraphs();
    }

    [Server]
    public void OnAllClientsCompletedLoadingGraphs()
    {
        Debug.Log(LOG_PREFIX + "OnAllClientsCompletedLoadingGraphs");
        StartMatch();
    }

    [Server]
    public void OnClientTriesToColorizeNode(int graphIndex, int nodeIndex, Color color, MPGCPlayerCoop player)
    {
        Debug.Log(LOG_PREFIX + "OnClientColorizedNode - player:" + player + " graph:"+graphIndex + " node:"+nodeIndex + " color:"+color);

        //colorize the node on all clients (including the host one).
        RpcClientTriesToColorizeNode(graphIndex, nodeIndex, color, player.netId.Value);
    }

    [Server]
    public void OnClientSuccessfullyColorizedNode(int graphIndex, MPGCPlayerCoop player)
    {
        GCGraph colorizedGraph = graphs[graphIndex];

        //check if graph completed (host graph is the controlling one).
        Color defaultColor = colorizedGraph.GetDefaultNodeColor();
        foreach (GCNode node in colorizedGraph.GetNodes())
            if (node.GetColor() == defaultColor)
                return;

        //graph completely colored
        RpcGraphCompleted(graphIndex, true, colorizedGraph.GetSolveDuration() - gameSlider.GetCurrentValue());

        //was it final graph?
        if (graphIndex == graphs.Count - 1)
            FinishMatch();
    }

    [Server]
    public void OnClientsFailedGraph()
    {
        int graphIndex = graphs.IndexOf(currentGraph);

        RpcGraphCompleted(graphIndex, false, currentGraph.GetSolveDuration());

        //was it final graph?
        if (graphIndex == graphs.Count - 1)
            FinishMatch();
    }

    [Server]
    public void OnClientClickedColorButton(Color clickedColor, MPGCPlayerCoop player)
    {
        //notify clients about a player changed his selected color
        RpcColorButtonClicked(clickedColor, player.netId.Value);
    }

    protected override int GetCountdownForLobby()
    {
        return COUNTDOWN_TO_RETURN_TO_LOBBY;
    }
    //////////////////////////////////////////////////////Server Logic End

    //////////////////////////////////////////////////////Client Logic Begin

    [ClientRpc]
    private void RpcReceiveGraphSelection(int[] graphDataIds)
    {
        Debug.Log(LOG_PREFIX + "RpcReceiveGraphSelection");
        
        List<GCGraphData> graphData = new List<GCGraphData>();
        for (int i = 0; i < graphDataIds.Length; i++)
        {
            graphData.Add( GCGraphData.GetGraphDataById(graphDataIds[i]) );
            Debug.Log("Received Graph Data: " + graphData[i].GetId());
        }
        StartCoroutine(LoadGraphsRoutine(graphData));
    }

    [Client]
    protected override void OnMatchStarted()
    {
        SetCurrentGraph(graphs[0]);
        gcUI.Show();
        screenFlasherUI.Show();
        gameSlider.Continue();
        audioPlayer.PlayGCMusic();

        //set player information
        foreach (MPGamePlayer gp in gamePlayers.Values)
        {
            //the MPGraphColoringUI currently supports only 2 players.
            //If there are more than one remote player, their visuals will be overriden
            if (gp.isLocalPlayer)
                gcUI.SetupLocalPlayerVisuals(gp);
            else
                gcUI.SetupRemotePlayerVisuals(gp);
        }
    }

    [Client]
    protected override void OnMatchFinished()
    {
        screenFlasherUI.Hide();
        gameSlider.Pause();
        gameOverviewUI.Show();
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        gcOverviewUI.ShowSolutions();
        audioPlayer.StopBgMusic();
    }

    void OnDestroy()
    {
        if (!isClient)
            return;

        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

        foreach (GCGraph graph in graphs)
        {
            graph.ResetColorOfAllNodes();
        }
    }

    [Client]
    void SetCursorColor(Color newColor)
    {
        if (null == paintBrushTexture)
            paintBrushTexture = Resources.Load<Texture2D>(Global.PATH_CURSOR_PAINTBRUSH);

        Texture2D newCursor = AssetUtils.CopyTexture(paintBrushTexture);
        AssetUtils.ReplaceTextureColor(newCursor, Color.white, newColor);
        Vector2 cursorSpot = new Vector2(0, newCursor.height);
        Cursor.SetCursor(newCursor, cursorSpot, CursorMode.Auto);
    }

    [Client]
    private IEnumerator LoadGraphsRoutine(List<GCGraphData> graphData)
    {
        graphs = new List<GCGraph>();
        for (int i = 0; i < graphData.Count; i++)
        {
            GCGraphData data = graphData[i];
            string graphName = data.GetGraphImageName();

            if (!gcLoader.GraphIsLoaded(graphName))
            {
                StartCoroutine(gcLoader.LoadGraph(data));
                yield return null;

                while (gcLoader.IsLoading())
                {
                    float currentLoadingProgress = gcLoader.GetLoadingProgress();
                    float overallProgress = (i + currentLoadingProgress) / graphData.Count;
                    loadingGameIndicator.UpdateProgress(overallProgress);
                    yield return null;
                }
            }
            GCGraph loadedGraph = gcLoader.GetGraphByName(graphName);
            if (null == loadedGraph)
                continue;

            graphs.Add(loadedGraph);
        }

        //loading complete. init overview and notify server
        gcOverviewUI.InitGraphResults(graphs.ToArray(), ShowGraphSolution);
        gcOverviewUI.HideSolutions();
        loadingGameIndicator.SetIndicationText(Global.STRING_INDICATE_WAITING_FOR_PLAYERS);
        loadingGameIndicator.SetShowProgressBar(false);
        GetLocalGCPlayer().CmdCompletedLoadingGraphs();
    }

    [Client]
    private void ShowGraphSolution(int graphIndex)
    {
        solutionDialog.SetImage(gcLoader.LoadGraphSolution(graphs[graphIndex].GetName()));
        solutionDialog.Show();
    }

    [Client]
    private void SetCurrentGraph(GCGraph graph)
    {
        Debug.Assert(graph.GetUsableColors().Length > 1);

        //TODO: maybe reset current graph / unload sprite to save memory
        currentGraph = graph;
        gcUI.SetGraph(currentGraph);
        gcUI.SetLevelProgressText(graphs.IndexOf(graph) + 1, graphs.Count);

        //set player graph colors depending on who is host. thus the colors will be equal on all clients.
        //remember: UI currently only supports two players. if there will be more players, this has to be controlled by server.
        Color[] availableColors = graph.GetUsableColors();
        int numOfHostColors = availableColors.Length / 2;
        int numOfNonHostColors = availableColors.Length - numOfHostColors;
        Color[] hostColors = new Color[numOfHostColors];
        Color[] nonHostColors = new Color[numOfNonHostColors];
        for(int i=0; i<hostColors.Length; i++)
            hostColors[i] = availableColors[i];
        for(int i=0; i<nonHostColors.Length; i++)
            nonHostColors[i] = availableColors[i + numOfHostColors];
        gcUI.SetLocalColorButtons(isServer ? hostColors : nonHostColors);
        gcUI.SetRemoteColorButtons(isServer ? nonHostColors : hostColors);

        //autoselect first color
        currentColor = isServer ? hostColors[0] : nonHostColors[0];
        SetCursorColor(currentColor);
        gcUI.SetColorButtonActive(currentColor);
        Color remoteColor = isServer ? nonHostColors[0] : hostColors[0];
        gcUI.SetRemoteColorButtonActive(remoteColor);

        gameSlider.Reset(currentGraph.GetSolveDuration());
        gameSlider.Continue();
    }

    [Client]
    private void NextGraph()
    {
        int currentIndex = graphs.IndexOf(currentGraph);
        currentIndex++;
        if (currentIndex >= graphs.Count)
            return;

        SetCurrentGraph(graphs[currentIndex]);
    }

    [ClientRpc]
    private void RpcColorButtonClicked(Color clickedColor, uint clickingPlayerId){

        Debug.Log(LOG_PREFIX + "RpcColorButtonClicked - player: " + clickingPlayerId + " - color: " + clickedColor);

        MPGamePlayer gp = GetGamePlayer(clickingPlayerId);
        if(gp.isLocalPlayer)
            return;

        //update remote color selection
        gcUI.SetRemoteColorButtonActive(clickedColor);
    }

    [Client]
    private void OnColorButtonClick(Color clickedColor)
    {
        SetCursorColor(clickedColor);
        currentColor = clickedColor;

        //notify server about client clicked a color button
        GetLocalGCPlayer().CmdColorButtonClicked(clickedColor);
    }

    [Client]
    private void OnEraseButtonClick()
    {
        OnColorButtonClick(currentGraph.GetDefaultNodeColor());
    }

    [Client]
    private void OnClearButtonClick()
    {
        currentGraph.ResetColorOfAllNodes();
        gameSlider.Reset();
    }

    [Client]
    private void OnGraphImageClick(PointerEventData data, RectTransform imageRect)
    {
        if (nodesAreFlickering || advancingToNextGraph)
            return;

        int xPos;
        int yPos;
        GetImageClickCoords(data, imageRect, out xPos, out yPos);

        GCNode clickedNode = currentGraph.GetNodeByPosition(xPos, yPos);
        if (null == clickedNode)
        {
            Debug.LogWarning("Could not find node at " + xPos + "/" + yPos);
            return;
        }

        //tell server that client tries to colorize the clicked node
        int currentGraphIndex = graphs.IndexOf(currentGraph);
        int clickedNodeIndex = currentGraph.GetNodes().IndexOf(clickedNode);
        Debug.Log(LOG_PREFIX + "Asking Server to colorize node " + clickedNodeIndex + " of graph " + currentGraphIndex);
        MPGCPlayerCoop localPlayer = GetLocalGCPlayer();
        localPlayer.CmdTryToColorizeNode(currentGraphIndex, clickedNodeIndex, currentColor);
    }

    [ClientRpc]
    public void RpcClientTriesToColorizeNode(int graphIndex, int nodeIndex, Color chosenColor, uint colorizingPlayerId)
    {
        Debug.Log(LOG_PREFIX + "RpcClientColorizedNode - " + colorizingPlayerId);
        MPGCPlayerCoop colorizingPlayer = (MPGCPlayerCoop)GetGamePlayer(colorizingPlayerId);

        GCNode clickedNode = graphs[graphIndex].GetNodes()[nodeIndex];

        //erase?
        Color defaultNodeColor = currentGraph.GetDefaultNodeColor();
        if (chosenColor == defaultNodeColor)
        {
            currentGraph.ResetColorOfNode(clickedNode);
            return;
        }

        //check color of neighbors to determine whether this action is allowed
        List<GCNode> neighborsWithSameColor = new List<GCNode>();
        foreach (GCNode neighbor in clickedNode.GetNeighbors())
        {
            if (neighbor.GetColor() != chosenColor)
                continue;

            //forbidden action! neighbor owns this color!
            neighborsWithSameColor.Add(neighbor);
        }

        //show flickering nodes to indicate that this was not allowed
        if (neighborsWithSameColor.Count > 0)
        {
            StartCoroutine(OnWrongColorRoutine(clickedNode, chosenColor, neighborsWithSameColor));

            //tell server that this player made a mistake to update syncvar
            if (colorizingPlayer.isLocalPlayer)
                colorizingPlayer.CmdMadeMistake();

            return;
        }

        //action allowed --> colorize node 
        currentGraph.ColorizeNode(clickedNode, chosenColor);

        //tell server, that this client successfully colorized a node
        if (colorizingPlayer.isLocalPlayer)
            colorizingPlayer.CmdColorizedNodeSuccessfully(graphIndex);
    }

    [ClientRpc]
    private void RpcGraphCompleted(int graphIndex, bool success, float requiredTime)
    {
        Debug.Log(LOG_PREFIX + "RpcGraphCompleted: " + graphIndex + " success:" + success);

        //update UI according to the success
        foreach (MPGamePlayer player in gamePlayers.Values)
        {
            MPGCPlayerCoop coopPlayer = player as MPGCPlayerCoop;
            gcOverviewUI.UpdateGraphResult(graphIndex, success, (UIGCPlayer)coopPlayer.GetUI());
            coopPlayer.IncreaseUsedTimeToSolve(requiredTime);

            if (success)
                coopPlayer.IncrementNumOfCorrectGraphs();
        }

        //advance to next graph, if this was current graph
        if (graphIndex != graphs.IndexOf(currentGraph))
            return;

        if (success)
            StartCoroutine(OnGraphCorrectRoutine());
        else
            StartCoroutine(OnGraphFailedRoutine());
    }

    [ClientRpc]
    private void RpcSyncTimer(float time)
    {
        gameSlider.SetTime(time);
    }

    [Client]
    private void GetImageClickCoords(PointerEventData data, RectTransform imageRect, out int xPos, out int yPos)
    {
        //determine clicked coords realtive to the image rect displayed on screen
        Vector2 localCursor;
        var pos1 = data.position;
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(imageRect, pos1, null, out localCursor))
        {
            xPos = -1;
            yPos = -1;
            return;
        }
        Rect rectBounds = imageRect.rect;
        xPos = (int)(localCursor.x);
        yPos = (int)(localCursor.y);
        xPos += (int)rectBounds.width / 2;
        yPos += (int)rectBounds.height / 2;

        //screen image size may differ from actual image size. adapt screen image click to real image click.
        //do this by determining percentual position of click, and applying this percentage to the real image.
        float xPerc = xPos / rectBounds.width;
        float yPerc = yPos / rectBounds.height;
        xPos = Mathf.RoundToInt(currentGraph.GetImage().texture.width * xPerc);
        yPos = Mathf.RoundToInt(currentGraph.GetImage().texture.height * yPerc);
    }

    [Client]
    private IEnumerator OnGraphCorrectRoutine()
    {
        advancingToNextGraph = true;
        gameSlider.Pause();
        yield return StartCoroutine(ShowCorrectFeedback());
        while (nodesAreFlickering)
            yield return null;
        NextGraph();
        advancingToNextGraph = false;
    }

    [Client]
    private IEnumerator OnGraphFailedRoutine()
    {
        advancingToNextGraph = true;
        gameSlider.Pause();
        yield return StartCoroutine(ShowWrongFeedback());
        while (nodesAreFlickering)
            yield return null;
        NextGraph();
        advancingToNextGraph = false;
    }

    [Client]
    private IEnumerator OnWrongColorRoutine(GCNode clickedNode, Color appliedColor, List<GCNode> neighborsWithSameColor)
    {
        yield return StartCoroutine(ShowWrongFeedback(clickedNode, appliedColor, neighborsWithSameColor));
    }

    [Client]
    private IEnumerator ShowCorrectFeedback()
    {
        audioPlayer.PlayCorrectSound();
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_GREEN));
        yield return new WaitForSeconds(1);
    }

    [Client]
    private IEnumerator ShowWrongFeedback(GCNode clickedNode, Color appliedColor, List<GCNode> neighborsWithSameColor)
    {
        audioPlayer.PlayWrongSound();
        StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_RED));


        if (neighborsWithSameColor.Count < 1)
            yield break;

        //highlight neighborsWithSameColor and temporarily show clicked node in selected color
        Color originalColor = clickedNode.GetColor();
        currentGraph.ColorizeNode(clickedNode, appliedColor);
        yield return StartCoroutine(FlickerNodes(neighborsWithSameColor, currentGraph.GetDefaultNodeColor(), appliedColor));
        currentGraph.ColorizeNode(clickedNode, originalColor);
    }

    [Client]
    private IEnumerator ShowWrongFeedback()
    {
        audioPlayer.PlayWrongSound();
        yield return StartCoroutine(screenFlasherUI.Flash(Global.COLOR_FLASH_RED));
    }

    [Client]
    private IEnumerator FlickerNodes(List<GCNode> nodes, Color flickerColorOne, Color flickerColorTwo)
    {
        nodesAreFlickering = true;

        //remember colors to re-apply after flickering finished
        Color[] originalColors = new Color[nodes.Count];
        for (int i = 0; i < nodes.Count; i++)
        {
            originalColors[i] = nodes[i].GetColor();
        }

        //toggle between flicker colors
        for (int i = 0; i < 8; i++)
        {
            Color highlightColor = i % 2 == 0 ? flickerColorOne : flickerColorTwo;
            for (int j = 0; j < nodes.Count; j++)
            {
                currentGraph.ColorizeNode(nodes[j], highlightColor);
            }
            yield return new WaitForSeconds(0.15f);
        }

        //re-apply colors
        for (int i = 0; i < nodes.Count; i++)
        {
            currentGraph.ColorizeNode(nodes[i], originalColors[i]);
        }

        nodesAreFlickering = false;
    }

    [Client]
    protected override UIGameOverview InitGameOverview()
    {
        gcOverviewUI = UIElement.Instantiate<UIGCOverview>(this.transform);
        return gcOverviewUI;
    }

    protected override void ShowGameOverviewOnTab()
    {
        if (null != GetLocalGCPlayer() && GetLocalGCPlayer().CompletedAllGraphs())
            return;

        base.ShowGameOverviewOnTab();
    }
    //////////////////////////////////////////////////////Client Logic End

    
}
