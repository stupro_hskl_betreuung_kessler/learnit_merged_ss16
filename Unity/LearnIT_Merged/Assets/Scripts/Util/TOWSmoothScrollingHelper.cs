﻿using UnityEngine;
using System.Collections;

/**
 * This class is used to make transitions of the TOW-slider be smooth instead of it jumping to the new value.
 */
public class TOWSmoothScrollingHelper
{
	private float lastScaling = 0.5F;
	private float targetScaling = 0.5F;
	private float scaleSpeed;
	private float startTime = -1;
	private bool reached = true;

	public TOWSmoothScrollingHelper(float scaleSpeed = 3F)
	{
		this.scaleSpeed = scaleSpeed;
	}

	public float getCurrentScale()
	{
		if (!reached) {
			CheckInitialized ();
			float deltaScaling = (targetScaling - lastScaling) * (Time.time - startTime) * scaleSpeed;
			float newScaling = lastScaling + deltaScaling;
			if (newScaling > lastScaling) {
				if (newScaling >= targetScaling) {
					reached = true;
					return targetScaling;
				} else {
					return newScaling;
				}
			} else if (newScaling == lastScaling) {
				return newScaling;
			} else {
				if (newScaling <= targetScaling) {
					reached = true;
					return targetScaling;
				} else {
					return newScaling;
				}
			}
		}
		return targetScaling;
	}

	public void updateTargetScaling(int scoreLocal, int scoreEnemy)
	{
		int difference = scoreLocal - scoreEnemy;
		lastScaling = getCurrentScale ();
		targetScaling = (3 + difference) / 6F;
		reached = false;
		resetTimer ();
	}

	private void resetTimer()
	{
		startTime = Time.time;
	}

	private void CheckInitialized()
	{
		if (startTime < 0)
		{
			resetTimer ();
		}
	}

	public bool finishedMoving()
	{
		return reached;
	}
}