﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IDragHandler
{
    private Transform transformToDrag = null;

    void Start()
    {
        //by default, set attached transform as dragable
        if (null == transformToDrag)
            transformToDrag = transform;
    }

    //can be used to actually move something although some other gameobject is being dragged.
    //e.g. drag only works on header, but whole window moves.
    public void SetTransformToDrag(Transform transformToDrag)
    {
        this.transformToDrag = transformToDrag;
    }

    public void OnDrag(PointerEventData eventData)
    {
        float newX = transformToDrag.position.x + eventData.delta.x;
        float newY = transformToDrag.position.y + eventData.delta.y;
        Vector3 newPos = new Vector3(newX, newY, transformToDrag.position.z);
        transformToDrag.position = newPos;
    }
}