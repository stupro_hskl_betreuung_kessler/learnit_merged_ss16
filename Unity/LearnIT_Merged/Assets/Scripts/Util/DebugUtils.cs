﻿#define DEBUGUTILS_ENABLED

using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System.Linq.Expressions;
using System;
using Debug = UnityEngine.Debug;

namespace Utils {

    static class DebugUtils
    {
        public static bool enabled = true;

        [Conditional("UNITY_EDITOR")]
        public static void Assert(bool condition, string message)
        {
#if UNITY_EDITOR && DEBUGUTILS_ENABLED
            if (enabled)
            {
                if (!condition)
                {
                    UnityEditor.EditorApplication.Beep();
                    Debug.LogError(message);
                }
            }
#endif
        }

        [Conditional("UNITY_EDITOR")]
        public static void AssertNotNull(string variableName, object objectToCheck)
        {
#if UNITY_EDITOR && DEBUGUTILS_ENABLED
            if (enabled)
            {
                if (null == objectToCheck)
                {
                    UnityEditor.EditorApplication.Beep();
                    Debug.LogError(variableName + " is null!");
                }
            }
#endif
        }

        public static string GenerateComponentMissingMsg<T>(Expression<Func<T>> memberExpression, Type t)
        {
            return ((MemberExpression)memberExpression.Body).Member.Name + " is missing a component: " + DebugUtils.GetClassName(t);
        }

        public static string GenerateComponentInChildrenMissingMsg<T>(Expression<Func<T>> memberExpression, Type t)
        {
            return "No child of " + ((MemberExpression)memberExpression.Body).Member.Name + " has the required component: " + DebugUtils.GetClassName(t);
        }

        public static string GetMemberName<T>(Expression<Func<T>> memberExpression)
        {
            MemberExpression expressionBody = (MemberExpression)memberExpression.Body;
            return expressionBody.Member.Name;
        }

        public static string GetClassName(System.Object o)
        {
            if (null == o)
                return "null";

            return o.GetType().Name;
        }

        public static string GetClassName(Type t)
        {
            return t.Name;
        }

        
    }
}

