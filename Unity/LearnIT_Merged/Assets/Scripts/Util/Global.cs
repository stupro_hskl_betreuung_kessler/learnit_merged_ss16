﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class Global {
    //miscellaneous Strings
    public static readonly string APPLICATION_NAME = "learnit"; //do not translate
    public static readonly string STRING_APPLICATION_TITLE = "LearnIT";
    public static readonly string STRING_PAUSE = "Pause";
    public static readonly string STRING_PLAY = "Spielen";
    public static readonly string STRING_CHALLENGE = "Herausfordern";
    public static readonly string STRING_GAME_MODE = "Spielmodus";
    public static readonly string STRING_GAME_MODE_INFO = "Beschreibung";
    public static readonly string STRING_PLACEHOLDER = "PLATZHALTER";
    public static readonly string STRING_SOLUTION = "Lösung";
    public static readonly string STRING_SHOW_SOLUTION = "Lösung anzeigen";
    public static readonly string STRING_HIDE_SOLUTION = "Lösung ausblenden";
    public static readonly string STRING_HELP = "Hilfe";
    public static readonly string STRING_OK = "OK";
    public static readonly string STRING_CANCEL = "Abbrechen";
    public static readonly string STRING_CLOSE = "Schließen";

    //Colors
    public static readonly Color COLOR_PINK = new Color(1.0f, 0.65f, 1.0f);
    public static readonly Color COLOR_FLASH_RED = new Color(0.77f, 0.27f, 0.27f);
    public static readonly Color COLOR_FLASH_GREEN = new Color(0.27f, 0.75f, 0.4f);
    public static readonly Color COLOR_DISABLED_GRAY = new Color(0.61f, 0.61f, 0.61f);
    public static readonly Color COLOR_INACTIVE_GRAY = new Color(0.85f, 0.85f, 0.85f, 0.8f);

    //Image Paths
    public static readonly string PATH_IMAGES = "Images" + Path.AltDirectorySeparatorChar;
    public static readonly string PATH_ICONS = PATH_IMAGES + "Icons" + Path.AltDirectorySeparatorChar;
    public static readonly string PATH_CP_ICONS = PATH_ICONS + "cp_gc_modes" + Path.AltDirectorySeparatorChar;
    public static readonly string PATH_QUIZ_IMAGES = PATH_IMAGES + "Quiz" + Path.AltDirectorySeparatorChar;
    public static readonly string PATH_ICON_IT_NUMBER_GUESSING_ARCADE = PATH_ICONS + "numberguessing_arcade";
    public static readonly string PATH_ICON_IT_NUMBER_GUESSING_SURVIVAL = PATH_ICONS + "numberguessing_survival";
    public static readonly string PATH_ICON_IT_NUMBER_GUESSING_MP_KOOP = PATH_ICONS + "numberguessing_mp";
    public static readonly string PATH_ICON_IT_NUMBER_GUESSING_MP_BLIND = PATH_ICONS + "numberguessing_mp_blind";
    public static readonly string PATH_ICON_QUIT = PATH_ICONS + "quit";
    public static readonly string PATH_ICON_EXIT = PATH_ICONS + "exit";
    public static readonly string PATH_ICON_INFO = PATH_ICONS + "info";
    public static readonly string PATH_ICON_WIN = PATH_ICONS + "win";
    public static readonly string PATH_ICON_LOSE = PATH_ICONS + "lose";
    public static readonly string PATH_ICON_ERROR = PATH_ICONS + "cancel";
    public static readonly string PATH_ICON_QUIZ = PATH_ICONS + "quiz";
    public static readonly string PATH_ICON_THEORY = PATH_ICONS + "book";
    public static readonly string PATH_ICON_QUIZ_MP = PATH_ICONS + "quiz_mp";
    public static readonly string PATH_ICON_PLAYER = PATH_ICONS + "player";
    public static readonly string PATH_ICON_PAUSE = PATH_ICONS + "pause";
    public static readonly string PATH_ICON_GAMEPAD = PATH_ICONS + "gamepad";
    public static readonly string PATH_ICON_QUESTIONMARK = PATH_ICONS + "questionmark";

    public static readonly string PATH_ICON_CP_GC_EASY = PATH_CP_ICONS + "cp_gc_graph";
    public static readonly string PATH_ICON_CP_GC_INTERMEDIATE = PATH_CP_ICONS + "cp_gc_logo";
    public static readonly string PATH_ICON_CP_GC_MEDIUM = PATH_CP_ICONS + "cp_gc_simplemap";
    public static readonly string PATH_ICON_CP_GC_HARD = PATH_CP_ICONS + "cp_gc_map";
    public static readonly string PATH_ICON_CP_GC_EXTREME = PATH_CP_ICONS + "cp_gc_max";
    public static readonly string PATH_ICON_CP_GC_EASY_MP = PATH_CP_ICONS + "cp_gc_graph_mp";
    public static readonly string PATH_ICON_CP_GC_INTERMEDIATE_MP = PATH_CP_ICONS + "cp_gc_logo_mp";
    public static readonly string PATH_ICON_CP_GC_HARD_MP = PATH_CP_ICONS + "cp_gc_map_mp";
    public static readonly string PATH_ICON_CP_GC_EXTREME_MP = PATH_CP_ICONS + "cp_gc_max_mp";
    public static readonly string PATH_ICON_CP_GC_RANDOM_MP = PATH_CP_ICONS + "cp_gc_random_mp";
    public static readonly string PATH_ICON_CP_GC_COOP_ONE = PATH_CP_ICONS + "cp_gc_coop1";
    public static readonly string PATH_ICON_CP_GC_COOP_TWO = PATH_CP_ICONS + "cp_gc_coop2";
    public static readonly string PATH_CURSOR_PAINTBRUSH = PATH_IMAGES + "paintbrush";


    //Stream Asset Paths
    public static readonly string PATH_STREAM_VIDEOS = "Video" + Path.AltDirectorySeparatorChar;
    public static readonly string PATH_TUTORIAL_NUMBER_ARCADE = PATH_STREAM_VIDEOS + "itnumberarcade";
    public static readonly string PATH_TUTORIAL_NUMBER_SURVIVAL = PATH_STREAM_VIDEOS + "itnumbersurvival";
    public static readonly string PATH_TUTORIAL_NUMBER_COOP = PATH_STREAM_VIDEOS + "itnumbercoop";
    public static readonly string PATH_TUTORIAL_NUMBER_COOP_BLIND = PATH_STREAM_VIDEOS + "itnumbercoopblind";
    public static readonly string PATH_TUTORIAL_QUIZ_SINGLE_PLAYER = PATH_STREAM_VIDEOS + "quizsp";
    public static readonly string PATH_TUTORIAL_QUIZ_MULTI_PLAYER = PATH_STREAM_VIDEOS + "quizmp";
    public static readonly string PATH_TUTORIAL_GC_COOP = PATH_STREAM_VIDEOS + "cpgraphcoloringcoop";
    public static readonly string PATH_TUTORIAL_GC_DUELL = PATH_STREAM_VIDEOS + "cpgraphcoloringduell";
    public static readonly string PATH_TUTORIAL_GC = PATH_STREAM_VIDEOS + "cpgraphcoloring";


    //Prefabs
    public static readonly string PATH_PREFABS_DIR = "Prefabs" + Path.AltDirectorySeparatorChar;

    //Indicator strings
    public static readonly string STRING_INDICATE_NO_CONNECTION = "Die Verbindung konnte nicht hergestellt werden";
    public static readonly string STRING_INDICATE_MATCH_DECLINED = "Deine Herausforderung wurde abgelehnt";
    public static readonly string STRING_INDICATE_TRYING_TO_CONNECT = "Die Verbindung wird hergestellt...";
    public static readonly string STRING_INDICATE_WAITING_FOR_RESPONSE = "Warte auf Antwort...";
    public static readonly string STRING_INDICATE_WAITING_FOR_PLAYERS = "Warte auf Mitspieler...";
    public static readonly string STRING_INDICATE_WAITING_FOR_INVITATION_RESPONSE = "Spieler wurde eingeladen. Warte auf Antwort...";
    public static readonly string STRING_INDICATE_ENTERING_LOBBY = "Betrete Lobby ...";
    public static readonly string STRING_INDICATE_RETURNING_TO_LOBBY = "Kehre zur Lobby zurück...";
    public static readonly string STRING_INDICATE_LOADING_GAME = "Lade Spiel ...";
    public static readonly string STRING_INDICATE_PLAYER_LEFT_LOBBY = "Mitspieler hat die Lobby verlassen";
    public static readonly string STRING_INDICATE_LOADING_QUIZ = "Lade Fragen herunter...";
    
    //Settings Strings
    public static readonly string STRING_SETTINGS_TITLE = "Einstellungen";
    public static readonly string STRING_SETTINGS_MUSICVOL = "Musik Lautstärke";
    public static readonly string STRING_SETTINGS_SFXVOL = "SFX Lautstärke";
    public static readonly string STRING_SETTINGS_MUTE = "Stummschalten";
    public static readonly string STRING_SETTINGS_EDIT_PLAYERNAME_TITLE = "Spielernamen ändern";
    public static readonly string STRING_SETTINGS_EDIT_PLAYERNAME_DESCRIPTION = "Du spielst momentan als" + Environment.NewLine + Environment.NewLine + 
                                                                    "\"{0}\"." + Environment.NewLine + Environment.NewLine +
                                                                    "Hier kannst du deinen Spielernamen ändern:";

    //Info Dialog Strings
    public static readonly string STRING_DIALOG_WIN_TITLE = "Gewonnen";
    public static readonly string STRING_DIALOG_LOSE_TITLE = "Verloren";
    public static readonly string STRING_DIALOG_TIME_UP_TITLE = "Zeit vorbei";
    public static readonly string STRING_DIALOG_WRONG_INPUT_TITLE = "Ungültige Eingabe";

    //Choose Opponent Dialog Strings
    public static readonly string STRING_DIALOG_CHOOSE_OPPONENT_TITLE = "Wähle einen Gegner";
    public static readonly string STRING_DIALOG_CHOOSE_OPPONENT_LIST_TITLE = "Spieler im Netzwerk";
    public static readonly string STRING_DIALOG_MATCH_INVITATION_TITLE = "Herausforderung erhalten";
    public static readonly string STRING_DIALOG_MATCH_INVITATION_DESCRIPTION = "\"{0}\" hat dich zu einer Runde \"{1}\" herausgefordert!" + Environment.NewLine + Environment.NewLine
                                                                               +"Nimmst du die Herausforderung an?";

    //Leave Lobby Dialog Strings
    public static readonly string STRING_DIALOG_LEAVE_LOBBY_TITLE = "Lobby wirklich verlassen?";
    public static readonly string STRING_DIALOG_LEAVE_LOBBY_DESCRIPTION = "Möchtest du die Lobby wirklich verlassen?";

    //Theory Strings
    public static readonly string STRING_THEORY_NAME = "Theorie";
    public static readonly string STRING_THEORY_DESCRIPTION = "Hier kannst du dich über die Grundlagen von \"{0}\" informieren." + Environment.NewLine + Environment.NewLine +
                                                              "Setze das hier erlangte Wissen ein um die verschiedenen Spielmodi von \"{0}\" zu meistern.";
    public static readonly string STRING_THEORY_TITLE = "Theorie zu \"{0}\"";
    public static readonly string STRING_THEORY_PAGE = "Seite";

    //Informationtheory Strings
    public static readonly string STRING_IT_TITLE = "Informationstheorie";
    public static readonly string STRING_IT_DIALOG_NUMBER_HINT = "Versuche systematisch vorzugehen. Beispielsweise mit der Binärsuche. Dort wird immer das mittlere Element der restlichen in Frage kommenden Elemente zuerst untersucht.";

    public static readonly string STRING_IT_NUMBER_ARCADE_NAME = "Zahlenraten Arcade";
    public static readonly string STRING_IT_NUMBER_ARCADE_DESCRIPTION = "Errate geheime Zahlen in möglichst wenigen Versuchen. " + Environment.NewLine + Environment.NewLine +
                                                                        "Aber sei gewarnt, die Zahlenbereiche werden immer größer!" + Environment.NewLine + Environment.NewLine +
                                                                        "Schaffst du es alle Zahlen zu erraten?";
    public static readonly string STRING_IT_NUMBER_ARCADE_HELP_TEXT = "Ziel des Spiels ist es die geheimen Zahlen der jeweiligen Level in möglichst wenigen Versuchen zu erraten. " +
                                                                      "Dabei liegen die gesuchten Zahlen immer innerhalb der angezeigten Grenzen (inklusive)." + Environment.NewLine + Environment.NewLine +
                                                                      "Hinweis: " + STRING_IT_DIALOG_NUMBER_HINT;

    public static readonly string STRING_IT_NUMBER_SURVIVAL_NAME = "Zahlenraten Überleben";
    public static readonly string STRING_IT_NUMBER_SURVIVAL_DESCRIPTION = "Errate in der vorgegebenen Zeit so viele Zahlen wie möglich." + Environment.NewLine + Environment.NewLine +
                                                                          "Rätst du richtig, wird deine Restzeit erhöht." + Environment.NewLine + Environment.NewLine +
                                                                          "Wie viele Zahlen kannst du erraten?";
    public static readonly string STRING_IT_NUMBER_SURVIVAL_HELP_TEXT = "Ziel des Spiels ist möglichst viele geheime Zahlen in der vorgegebenen Zeit zu erraten. " +
                                                                      "Dabei liegen die gesuchten Zahlen immer innerhalb der angezeigten Grenzen (inklusive)." + Environment.NewLine + Environment.NewLine +
                                                                      "Hinweis: " + STRING_IT_DIALOG_NUMBER_HINT;

    public static readonly string STRING_IT_QUIZ_NAME = "Informationstheorie Quiz";
    public static readonly string STRING_IT_QUIZ_DESCRIPTION = "Stelle dein Wissen im Quiz auf die Probe." + Environment.NewLine + Environment.NewLine +
                                                                        "Fragen rund aus dem Gebiet der Informationstheorie erwarten dich." + Environment.NewLine + Environment.NewLine +
                                                                        "Wie gut kennst du dich damit aus?";
    public static readonly string STRING_IT_NUMBER_SURVIVAL_MP_NAME = "Zahlenraten Kooperativ";
    public static readonly string STRING_IT_NUMBER_SURVIVAL_MP_DESCRIPTION = "Erratet zusammen so viele Zahlen wie möglich bevor euch die Zeit davon läuft." + Environment.NewLine + Environment.NewLine +
                                                                          "Rät einer von euch richtig, wird eure Restzeit erhöht." + Environment.NewLine + Environment.NewLine +
                                                                          "Wie viele Zahlen könnt ihr zusammen erraten? Helft euch gegenseitig und überlegt euch eine Taktik.";
    public static readonly string STRING_IT_NUMBER_BLIND_SURVIVAL_MP_NAME = "Zahlenraten Blindes Huhn";
    public static readonly string STRING_IT_NUMBER_BLIND_SURVIVAL_MP_DESCRIPTION = "Erratet zusammen so viele Zahlen wie möglich bevor euch die Zeit davon läuft." + Environment.NewLine + Environment.NewLine +
                                                                          "Vorsicht! Ihr seht jeweils nur die Versuche des Mitspielers!" + Environment.NewLine + Environment.NewLine +
                                                                          "Schafft ihr es euch gegenseitig zu den richtigen Zahlen zu führen?";
    public static readonly string STRING_IT_QUIZ_MP_NAME = "Infomartionstheorie Quiz - Duell";
    public static readonly string STRING_IT_QUIZ_MP_DESCRIPTION = "Messe dich im Quiz-Duell mit einem anderen Spieler." + Environment.NewLine + Environment.NewLine +
                                                                  "Fragen rund aus dem Gebiet der Informationstheorie erwarten euch." + Environment.NewLine + Environment.NewLine +
                                                                  "Wer kennt sich besser damit aus?";
    public static readonly string STRING_IT_NUMBER_TOO_LOW = "Zu Niedrig";
    public static readonly string STRING_IT_NUMBER_TOO_HIGH = "Zu Hoch";

    public static readonly string STRING_IT_DIALOG_NUMBER_ARCADE_WIN_DESCRIPTION = "Super! Du hast alle Level geschafft!";
    
    public static readonly string STRING_IT_DIALOG_NUMBER_ARCADE_LOSE_DESCRIPTION = "Du hast leider zu oft daneben gelegen." + Environment.NewLine + Environment.NewLine + STRING_IT_DIALOG_NUMBER_HINT;
    public static readonly string STRING_IT_DIALOG_NUMBER_ARCADE_WRONG_INPUT_DESCRIPTION = "Bitte eine ganze Zahl zwischen {0} und {1} eingeben";
    public static readonly string STRING_IT_DIALOG_NUMBER_SURVIVAL_TIME_UP_DESCRIPTION = "Zeit vorbei. Du hast in der vorgegebenen Zeit {0} Zahlen erraten. Kannst du dich noch verbessern?";

    //Complexity Strings
    public static readonly string STRING_CP_TITLE = "Komplexitätstheorie";
    public static readonly string STRING_CP_GC_DIALOG_ARCADE_WIN_DESCRIPTION = "Super! Du hast alle Graphen im Modus \"{0}\" gelöst!";
    public static readonly string STRING_CP_GC_DIALOG_HINT = "Denke daran: Zwei direkt benachbarte Flächen müssen in unterschiedlichen Farben eingefärbt werden.";
    public static readonly string STRING_CP_GC_DIALOG_ARCADE_LOSE_DESCRIPTION = "Schade, du hast leider zu oft falsch eingefärbt." + Environment.NewLine + Environment.NewLine + STRING_CP_GC_DIALOG_HINT;
    public static readonly string STRING_CP_GC_DIALOG_TIME_UP_DESCRIPTION = "Schade, die Zeit ist abgelaufen bevor du alle Flächen des Graphen einfärben konntest." + Environment.NewLine + Environment.NewLine + STRING_CP_GC_DIALOG_HINT;

    public static readonly string STRING_CP_GC_EXPLANATION = "Löse die Rätsel indem du alle Flächen einfärbst." + Environment.NewLine + Environment.NewLine +
                                                             "Wichtig: Direkte Nachbarflächen müssen in unterschiedlichen Farben eingefärbt werden." + Environment.NewLine + Environment.NewLine +
                                                             "Hinweis: Jedes Level stellt genau so viele Farben zur Verfügung, wie für das Lösen des Levels benötigt werden.";
    public static readonly string STRING_CP_GC_HELP_TEXT = STRING_CP_GC_EXPLANATION + Environment.NewLine + Environment.NewLine +
                                                      "Wenn du nicht weiter weißt, kannst du dir die Lösung des Levels ansehen. Möchtest du dir die Lösung ansehen?";


    public static readonly string STRING_CP_GC_EASY = "Einfache Graphen";
    public static readonly string STRING_CP_GC_EASY_DESCRIPTION = "Schwierigkeit: Einfach" + Environment.NewLine +
                                                                  "Modus: Zeit" + Environment.NewLine + Environment.NewLine +
                                                                  STRING_CP_GC_EXPLANATION;
    public static readonly string STRING_CP_GC_INTERMEDIATE = "Bekannte Logos";
    public static readonly string STRING_CP_GC_INTERMEDIATE_DESCRIPTION = "Schwierigkeit: Mittel" + Environment.NewLine +
                                                                          "Modus: Zeit" + Environment.NewLine + Environment.NewLine +
                                                                          STRING_CP_GC_EXPLANATION;
    public static readonly string STRING_CP_GC_MEDIUM = "Einfache Karten";
    public static readonly string STRING_CP_GC_MEDIUM_DESCRIPTION = "Schwierigkeit: Mittel" + Environment.NewLine +
                                                                    "Modus: Versuche" + Environment.NewLine + Environment.NewLine +
                                                                    STRING_CP_GC_EXPLANATION;
    public static readonly string STRING_CP_GC_HARD = "Komplexe Karten";
    public static readonly string STRING_CP_GC_HARD_DESCRIPTION = "Schwierigkeit: Schwer" + Environment.NewLine +
                                                                  "Modus: Versuche" + Environment.NewLine + Environment.NewLine +
                                                                  STRING_CP_GC_EXPLANATION;
    public static readonly string STRING_CP_GC_EXTREME = "Meisterlevel";
    public static readonly string STRING_CP_GC_EXTREME_DESCRIPTION = "Schwierigkeit: Extrem" + Environment.NewLine +
                                                                     "Modus: Versuche" + Environment.NewLine + Environment.NewLine +
                                                                     STRING_CP_GC_EXPLANATION;

    public static readonly string STRING_CP_GC_EASY_MP = "Duell: Graphenfärben";
    public static readonly string STRING_CP_GC_EASY_DESCRIPTION_MP = "Gegeneinander" + Environment.NewLine + 
                                                                     "Schwierigkeit: Einfach" + Environment.NewLine +
                                                                     "Modus: Zeit" + Environment.NewLine + Environment.NewLine +
                                                                     STRING_CP_GC_EXPLANATION;
    public static readonly string STRING_CP_GC_INTERMEDIATE_MP = "Duell: Logofärben";
    public static readonly string STRING_CP_GC_INTERMEDIATE_DESCRIPTION_MP = "Gegeneinander" + Environment.NewLine + 
                                                                             "Schwierigkeit: Mittel" + Environment.NewLine +
                                                                             "Modus: Zeit" + Environment.NewLine + Environment.NewLine +
                                                                             STRING_CP_GC_EXPLANATION;
    public static readonly string STRING_CP_GC_HARD_MP = "Duell: Kartenfärben";
    public static readonly string STRING_CP_GC_HARD_DESCRIPTION_MP = "Gegeneinander" + Environment.NewLine + 
                                                                     "Schwierigkeit: Schwer" + Environment.NewLine +
                                                                     "Modus: Zeit" + Environment.NewLine + Environment.NewLine +
                                                                     STRING_CP_GC_EXPLANATION;
    public static readonly string STRING_CP_GC_EXTREME_MP = "Duell: Meisterfärben";
    public static readonly string STRING_CP_GC_EXTREME_DESCRIPTION_MP = "Gegeneinander" + Environment.NewLine + 
                                                                        "Schwierigkeit: Extrem" + Environment.NewLine +
                                                                        "Modus: Zeit" + Environment.NewLine + Environment.NewLine +
                                                                        STRING_CP_GC_EXPLANATION;
    public static readonly string STRING_CP_GC_RANDOM_MP = "Duell: Zufallsfärben";
    public static readonly string STRING_CP_GC_RANDOM_DESCRIPTION_MP = "Gegeneinander" + Environment.NewLine +
                                                                        "Schwierigkeit: Zufall" + Environment.NewLine +
                                                                        "Modus: Zeit" + Environment.NewLine + Environment.NewLine +
                                                                        STRING_CP_GC_EXPLANATION;

    public static readonly string STRING_CP_GC_COOP_EXPLANATION = "In diesem Modus musst du mit deinem Mitspieler zusammenarbeiten!" + Environment.NewLine +
                                                                  "Ihr besitzt jeweils nur die Hälfte der benötigten Farben, aber gemeinsam verfügt ihr über alle Farben." + Environment.NewLine +
                                                                  "Platziert eure Farben so, dass die direkt benachbarten Felder vom anderen Spieler eingefärbt werden können." + Environment.NewLine +
                                                                  "Sprecht euch ab, dann könnt ihr das Farbenproblem gemeinsam lösen!";

    public static readonly string STRING_CP_GC_COOP_ONE_MP = "Koop: Eine Farbe";
    public static readonly string STRING_CP_GC_COOP_ONE_DESCRIPTION_MP =  "Kooperativ" + Environment.NewLine +
                                                                        "Je eine Farbe" + Environment.NewLine +
                                                                        "Schwierigkeit: Zufall" + Environment.NewLine +
                                                                        "Modus: Zeit" + Environment.NewLine + Environment.NewLine +
                                                                        STRING_CP_GC_COOP_EXPLANATION;
    public static readonly string STRING_CP_GC_COOP_TWO_MP = "Koop: Zwei Farben";
    public static readonly string STRING_CP_GC_COOP_TWO_DESCRIPTION_MP = "Kooperativ" + Environment.NewLine +
                                                                        "Je zwei Farben" + Environment.NewLine +
                                                                        "Schwierigkeit: Zufall" + Environment.NewLine +
                                                                        "Modus: Zeit" + Environment.NewLine + Environment.NewLine +
                                                                        STRING_CP_GC_COOP_EXPLANATION;

    public static readonly string STRING_CP_QUIZ_NAME = "Komplexitätstheorie Quiz";
    public static readonly string STRING_CP_QUIZ_DESCRIPTION = "Stelle dein Wissen im Quiz auf die Probe." + Environment.NewLine + Environment.NewLine +
                                                                "Fragen rund aus dem Gebiet der Komplexitätstheorie erwarten dich." + Environment.NewLine + Environment.NewLine +
                                                                "Wie gut kennst du dich damit aus?";
    public static readonly string STRING_CP_QUIZ_MP_NAME = "Komplexitätstheorie Quiz - Duell";
    public static readonly string STRING_CP_QUIZ_MP_DESCRIPTION = "Messe dich im Quiz-Duell mit einem anderen Spieler." + Environment.NewLine + Environment.NewLine +
                                                                  "Fragen rund aus dem Gebiet der Komplexitätstheorie erwarten euch." + Environment.NewLine + Environment.NewLine +
                                                                  "Wer kennt sich besser damit aus?";

    //custom game strings
    public static readonly string STRING_CUSTOM_TITLE = "Individuelles Thema";
    public static readonly string STRING_CUSTOM_QUIZ_NAME = "Individuelles Quiz";
    public static readonly string STRING_CUSTOM_QUIZ_DESCRIPTION = "Stelle dein Wissen im Quiz auf die Probe." + Environment.NewLine + Environment.NewLine +
                                                                    "Fragen aus individuellen Themen erwarten dich.";
    public static readonly string STRING_CUSTOM_QUIZ_MP_NAME = "Individuelles Quiz-Duell";
    public static readonly string STRING_CUSTOM_QUIZ_MP_DESCRIPTION = "Messe dich im Quiz-Duell mit einem anderen Spieler." + Environment.NewLine + Environment.NewLine +
                                                                      "Fragen aus individuellen Themen erwarten euch.";
    public static readonly string STRING_QUIZ_HELP_TEXT = "Ziel des Spiels ist es, möglichst viele der gestellten Fragen korrekt zu beantworten." + Environment.NewLine + Environment.NewLine +
                                                          "Zu jeder Frage gibt es genau vier Antwortmöglichkeiten, wovon genau eine richtig ist." + Environment.NewLine + Environment.NewLine +
                                                          "Außerdem verfügt jede Frage über ihre eigene Lösungsdauer, in der sie beantwortet werden muss bevor sie automatisch als \"Falsch beantwortet\" gewertet wird.";
                                                       
    //Lobby Strings
    public static readonly string STRING_LOBBY_TITLE = "Lobby";
    public static readonly string STRING_LOBBY_CHAT_TITLE = "Chat";
    public static readonly string STRING_LOBBY_CHAT_SEND = "Senden";
    public static readonly string STRING_LOBBY_CHAT_PLACEHOLDER = "Schreibe eine Nachricht...";
    public static readonly string STRING_LOBBY_PLAYER_LIST_TITLE = "Spieler";
    public static readonly string STRING_LOBBY_PLAYER_COLOR = "Farbe";
    public static readonly string STRING_LOBBY_PLAYER_NAME = "Name";
    public static readonly string STRING_LOBBY_PLAYER_READY = "Bereit";
    public static readonly string STRING_LOBBY_LEAVE = "Verlassen";

    //Game Overview Strings
    public static readonly string STRING_OVERVIEW_TITLE = "Übersicht";
    public static readonly string STRING_OVERVIEW_COLOR = "Farbe";
    public static readonly string STRING_OVERVIEW_NAME = "Spieler";
    public static readonly string STRING_OVERVIEW_BACK_TO_LOBBY = "Zur Lobby";
    public static readonly string STRING_OVERVIEW_RETURNING_TO_LOBBY = "Zurück zur Lobby in {0}s ...";
    public static readonly string STRING_OVERVIEW_GUESSES = "Versuche";
    public static readonly string STRING_OVERVIEW_CORRECT_GUESSES = "Davon Korrekt";
    public static readonly string STRING_OVERVIEW_BONUS_TIME = "Erspielte Bonuszeit";
    public static readonly string STRING_OVERVIEW_LEVEL = "Erreichtes Level";
    public static readonly string STRING_OVERVIEW_CORRECT = "Korrekt";
    public static readonly string STRING_OVERVIEW_QUESTION = "Frage";
    public static readonly string STRING_OVERVIEW_WAITING_FOR_PLAYER = "Warte auf Mitspieler...";
    public static readonly string STRING_OVERVIEW_QUIZ_QUESTION = "Frage";

    //LEARN IT 2 Constants//////////////////////////////////////////////////////////////////////////////////////
    //Game Ending Texts
    public const string END_TEXT_ARCADE = "Du hast leider keine Versuche mehr. Versuche es doch noch einmal!";
    public const string END_TEXT_NUMBERSYSTEMS_COOP_0 = "Sehr gut! Ihr habt Level ";
    public const string END_TEXT_NUMBERSYSTEMS_COOP_1 = " erreicht!";
    public const string WELL_DONE_TEXT = "Gut gemacht! Du hast alle Level bestanden!";
    public const string LOST_TEXT = "VERLOREN!";
    public const string WIN_TEXT = "GEWONNEN!";
    public const string YOU_WIN_TEXT = "DU GEWINNST!";
    public const string YOU_LOSE_TEXT = "DU VERLIERST!";
    public const string LEVEL_REACHED_TEXT = "Die Zeit ist abgelaufen. Du hast LEVEL {0} erreicht! Gut gemacht! Versuche es doch noch einmal!";
    //Game Titel Texts
    public const string GAME_TITLE_ARCADE = "Binärzahlen Umrechnen Arcade";
    public const string GAME_TITLE_SURVIVAL = "Binärzahlen Umrechnen Survival";
    public const string NUMBERSYSTEMS_COOP_TITLE_TEXT = "Zahlensysteme Umrechnen - Coop";
    public const string NUMBERSYSTEMS_VS_TITLE_TEXT = "Zahlensysteme Umrechnen - Duell";
    public const string MODULO_VS_TITEL_TEXT = "Modulo";
    //Number Systems Texts
    public const string BINARY_TEXT = "Binär";
    public const string OCTAL_TEXT = "Oktal";
    public const string DECIMAL_TEXT = "Dezimal";
    public const string HEX_TEXT = "Hexadezimal";
    //Game Modes Texts
    public const string DUEL_TEXT = " Duell";
    public const string TOW_TEXT = " Tauziehen";
    public const string ARCADE_TEXT = "Arcade";
    public const string SURVIVAL_TEXT = "Survival";
    //other
    public const string LEVEL_TEXT = "LEVEL";
    public const string ROUND_TEXT = "Runde";
    public const string ANSWER_TEXT = "Antwort";
    public const string ASKED_TEXT = "Gefragt";
    public const string CORRECT_TEXT = "Korrekt";

    //Numbers
    public const int START_TIME = 7;
    public const int TIME_BONUS = 3;
    public const int START_LEVEL = 1;
    public const int MAX_LEVEL = 12;
    public const int MAX_TRIES = 2;
    public const int TOW_WINNING_LEAD = 3;
    public const int DUEL_MAX_SCORE = 5;
    public const int MAX_TRIES_COOP = 3;
    public const int MAX_SWAPS_COOP = 3;

    //Colors
    public static readonly Color32 STANDARD_BLUE = new Color32(0, 69, 149, 255);
    public static readonly Color DISABLED_INPUTFIELD_COLOR = new Color(0.92f, 0.92f, 0.92f);
}
