﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GCNode {

    public struct Point
    {
        public int x;
        public int y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    bool[][] hasPixel;
    List<Point> pixels;
    List<Point> borderPixels;
    List<GCNode> neighbors;

    Color color;

    public GCNode(GCGraph parentGraph)
    {
        pixels = new List<Point>();
        borderPixels = new List<Point>();
        neighbors = new List<GCNode>();

        parentGraph.AddNode(this);
        int imageWidth = parentGraph.GetImage().texture.width;
        int imageHeight = parentGraph.GetImage().texture.height;
        hasPixel = new bool[imageWidth][];
        for (int i = 0; i < imageWidth; i++)
            hasPixel[i] = new bool[imageHeight];
    }

    public void SetColor(Color color){
        this.color = color;
    }

    public Color GetColor(){
        return this.color;
    }

    public void AddPixel(int x, int y)
    {
        pixels.Add(new Point(x, y));
    }

    public void AddPixel(Point pixel)
    {
        pixels.Add(pixel);
        hasPixel[pixel.x][pixel.y] = true;
    }

    public void AddBorderPixel(Point pixel)
    {
        borderPixels.Add(pixel);
    }

    public List<Point> GetPixels()
    {
        return pixels;
    }

    public bool HasPixel(Point pixel)
    {
        return hasPixel[pixel.x][pixel.y];
    }

    public List<Point> GetBorder()
    {
        return borderPixels;
    }

    public bool IsNeighborTo(GCNode potentialNeighbor){
        return neighbors.Contains(potentialNeighbor);
    }

    public void AddNeighbor(GCNode neighbor)
    {
        if (neighbor==null || neighbors.Contains(neighbor) || this.Equals(neighbor))
            return;

        neighbors.Add(neighbor);
        neighbor.AddNeighbor(this);
    }

    public List<GCNode> GetNeighbors()
    {
        return neighbors;
    }

    public override string ToString()
    {
        return "[Color]=" + color + " | " + "[Pixels]=" + pixels.Count + " | " + "[Border]=" + borderPixels.Count;
    }
}
