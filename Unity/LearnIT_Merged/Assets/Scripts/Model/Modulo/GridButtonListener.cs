﻿using UnityEngine;
using System.Collections;

public interface GridButtonListener {

	void Listen(GridInputField field);
}
