﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Utils;

public class Quiz {
    private List<QuizQuestion> questions;
    private float defaultSolveDuration;
    private int questionsPerMatch;
    private bool shuffleQuestions;

    public Quiz(int questionsPerMatch, bool shuffleQuestions, float defaultSolveDuration)
    {
        this.questionsPerMatch = questionsPerMatch;
        this.shuffleQuestions = shuffleQuestions;
        this.defaultSolveDuration = defaultSolveDuration;
        this.questions = new List<QuizQuestion>();
    }

    public void AddQuestion(QuizQuestion question)
    {
        if (null == question)
            return;

        if (question.GetSolveDuration() < 1)
            question.SetSolveDuration(defaultSolveDuration);

        questions.Add(question);
    }

    public QuizQuestion[] GetQuestions()
    {
        //reset questions, since they might still have the answerwed state from a previous round
        foreach (QuizQuestion qq in questions)
            qq.Reset();

        if (shuffleQuestions)
            questions.Shuffle();

        //if questionsPerMatch was not specified or is greater than the amount of questions, simply return all questions
        if (questionsPerMatch < 1 || questionsPerMatch >= questions.Count)
            return questions.ToArray();

        //fill a array of size [questionsPerMatch]. If questions have been shuffled before, this will equal a random selection
        QuizQuestion[] questionArray = new QuizQuestion[questionsPerMatch];
        for (int i = 0; i < questionsPerMatch; i++)
        {
            questionArray[i] = questions[i];
        }
        return questionArray;
    }

}
