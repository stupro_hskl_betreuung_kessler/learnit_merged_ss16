﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class NetworkClientInfo : LocalClientInfo, System.IComparable<NetworkClientInfo>
{

    string ip;
    float latestUpdateReceived;

    public NetworkClientInfo(string id, string playerName, bool isPlaying, string ip)
        : base(id, playerName, isPlaying)
    {
        SetIP(ip);
        UpdateTimer();
    }

    public void Update(string playerName, bool isPlaying, string ip)
    {
        SetPlayerName(playerName);
        SetIsPlaying(isPlaying);
        SetIP(ip);
        UpdateTimer();
    }

    public void UpdateTimer()
    {
        this.latestUpdateReceived = Time.time;
    }

    public float GetLatestUpdateReceived()
    {
        return latestUpdateReceived;
    }

    public void SetIP(string ip)
    {
        this.ip = ip;
    }

    public string GetIP(){
        return ip;
    }

    public override string ToString()
    {
        return "id:"+id + " playerName:" + playerName + " isPlaying:" + isPlaying + " ip:" + ip + " latestUpdate:" + latestUpdateReceived;
    }

    public bool UpdateRequired(string playerName, bool isPlaying, string ip)
    {
        bool hasNewName = !this.playerName.Equals(playerName);
        bool hasNewPlayingState = this.isPlaying != isPlaying;
        bool hasNewIP = !this.ip.Equals(ip);
        return hasNewName || hasNewPlayingState || hasNewIP;
    }

    //to sort clientinfos by name
    public int CompareTo(NetworkClientInfo other)
    {
        if (null == other)
            return 1;

        return playerName.CompareTo(other.playerName);
    }  
}
