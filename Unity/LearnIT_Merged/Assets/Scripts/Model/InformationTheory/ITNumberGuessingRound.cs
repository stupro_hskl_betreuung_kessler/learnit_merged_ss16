﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class ITNumberGuessingRound {
    private static readonly System.Random RANDOM_GEN = new System.Random();
    public static readonly ReadOnlyCollection<ITNumberGuessingRound> GUESSING_ROUNDS;
    static ITNumberGuessingRound()
    {
        List<ITNumberGuessingRound> tmp = new List<ITNumberGuessingRound>();
        tmp.Add(new ITNumberGuessingRound(1, 3, true));
        tmp.Add(new ITNumberGuessingRound(1, 7, true));
        tmp.Add(new ITNumberGuessingRound(1, 15, true));
        tmp.Add(new ITNumberGuessingRound(1, 31, true));
        tmp.Add(new ITNumberGuessingRound(1, 63, true));
        tmp.Add(new ITNumberGuessingRound(1, 127, true));
        tmp.Add(new ITNumberGuessingRound(1, 255, true));
        tmp.Add(new ITNumberGuessingRound(1, 511, true));
        tmp.Add(new ITNumberGuessingRound(1, 1023, true));
        GUESSING_ROUNDS = tmp.AsReadOnly();
    }

    public static ITNumberGuessingRound GenerateRoundByLevelProgress(int level)
    {
        int rangeEnd = level <= 1 ? 2 : level * level;
        return new ITNumberGuessingRound(1, rangeEnd);
    }

    public enum GuessingHint : int
    {
        TOO_LOW = -1,
        CORRECT = 0,
        TOO_HIGH = 1,
        WRONG_INPUT = 2
    }

    private int rangeStart;
    private int rangeEnd;
    private int numberToGuess;
    private int numOfGuesses;
   
    private bool numOfGuessesIsLimited;
    private int maxAllowedGuesses;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="rangeStart">minimum value inclusive</param>
    /// <param name="rangeEnd">maximum value inclusive</param>
    public ITNumberGuessingRound(int rangeStart, int rangeEnd)
    {
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
        this.numberToGuess = GetRandomInRange();
        this.numOfGuesses = 0;

        numOfGuessesIsLimited = false;
        maxAllowedGuesses = 0;
    }

    public ITNumberGuessingRound(int rangeStart, int rangeEnd, int maxAllowedGuesses)
        :this(rangeStart, rangeEnd)
    {
        this.numOfGuessesIsLimited = true;
        this.maxAllowedGuesses = maxAllowedGuesses;
    }

    public ITNumberGuessingRound(int rangeStart, int rangeEnd, bool limitGuessesToMinimum)
        :this(rangeStart, rangeEnd)
    {
        if (!limitGuessesToMinimum)
            return;
        
        this.numOfGuessesIsLimited = true;
        this.maxAllowedGuesses = GetMinNumOfNeededGuesses();
    }

    private int GetRandomInRange()
    {
        //+1 to include rangeEnd
        return RANDOM_GEN.Next(rangeStart, rangeEnd+1);
    }

    public int GetRangeStart()
    {
        return rangeStart;
    }

    public int GetRangeEnd()
    {
        return rangeEnd;
    }

    public int GetNumberToGuess()
    {
        return numberToGuess;
    }

    public void SetNumberToGuess(int numToGuess)
    {
        Debug.Assert(numToGuess >= rangeStart && numToGuess <= rangeEnd);
        this.numberToGuess = numToGuess;
    }

    public int GetMaxAllowedGuesses()
    {
        return maxAllowedGuesses;
    }

    /**
        * Returns the amount of guesses typically needed
        * to find the numberToGuess when halving the range each time
        */
    public int GetMinNumOfNeededGuesses()
    {   
        return Mathf.CeilToInt(Mathf.Log(rangeEnd - rangeStart + 1, 2));
    }

    public int GetNumOfGuesses()
    {
        return numOfGuesses;
    }

    public bool NumOfGuessesIsLimited()
    {
        return numOfGuessesIsLimited;
    }

    /**
     * Checks wether the given string represents the numberToGuess.
     * Returns an appropriate GuessingHint.
     */
    public GuessingHint CheckGuess(string inputText)
    {
        int guessedNumber = 0;
        bool tryParse = int.TryParse(inputText, out guessedNumber);
        if (!tryParse)
            return GuessingHint.WRONG_INPUT;

        numOfGuesses++;
        int numToGuess = this.GetNumberToGuess();
        if (guessedNumber == numToGuess)
            return GuessingHint.CORRECT;

        return guessedNumber < numToGuess ? GuessingHint.TOO_LOW : GuessingHint.TOO_HIGH;
    } 

    public void GenerateNewRandomNumber(){
        numberToGuess = GetRandomInRange();
    }
}
