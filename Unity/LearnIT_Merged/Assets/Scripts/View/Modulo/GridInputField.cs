﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GridInputField : UIElement
{
	Button gridButton;

	[SerializeField]
	private InputField gridInputField;
    [SerializeField]
    private Image glow;
	[SerializeField]
	private Image glowEnemy;

	private int position = -1;
	private UINumPad numpad;
	private GridButtonListener listener;

	public InputField GetInputField(){
		return gridInputField;
	}

    public void SetGlowing(bool glow)
    {
        this.glow.enabled = glow;
    }

	public string GetText()
	{
		return gridInputField.text;
	}

	public void SetNumpad(UINumPad numpad){

		this.numpad = numpad;
	}

	public int Position{
		get{
			return position;
		}
		set{
			position = value;
		}
	}

	public Button GetButton(){
		return gridButton;
	}

	public void SetEnabled(bool enabled){

		gridButton.interactable = enabled;
		ColorBlock block = gridInputField.colors;
		block.disabledColor = new Color (0.92f, 0.92f, 0.92f);
		gridInputField.colors = block;
	}

	public void SetCursorEnemy(bool enemyGlow)
	{
		this.glowEnemy.enabled = enemyGlow;
	}

	 void Awake(){

		Button prefabButton = UnityEngine.Resources.Load<Button> ("Prefabs/GridButton");
		gridButton = (Button)Instantiate (prefabButton);
		gridButton.transform.parent = this.transform;
		gridInputField.interactable = false;
		gridButton.onClick.AddListener (SelectFieldListener);
        glow.enabled = false;
		glowEnemy.enabled = false;

	}
		
	public void SelectFieldListener(){
		if (listener != null && numpad != null) {
			numpad.SetInputField (gridInputField);
			listener.Listen (this);
			ColorBlock block = gridInputField.colors;
			block.disabledColor = new Color (1.0f, 1f, 1f);
			gridInputField.colors = block;
            SetGlowing(true);
		}
	}

	public void Deselect(){

		ColorBlock block = gridInputField.colors;
        block.disabledColor = Global.DISABLED_INPUTFIELD_COLOR;
		gridInputField.colors = block;
	}

	public void SetListener(GridButtonListener listener){

		this.listener = listener;
	}
}