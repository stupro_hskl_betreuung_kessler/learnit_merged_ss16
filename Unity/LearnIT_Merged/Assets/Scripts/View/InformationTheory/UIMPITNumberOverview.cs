﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
public class UIMPITNumberOverview : UIGameOverview {

    [SerializeField] private Text colTextGuesses;
    [SerializeField] private Text colTextCorrectGuesses;
    [SerializeField] private Text colTextBonusTime;
    [SerializeField] private Text colTextLevel;

    protected override void Awake()
    {
        base.Awake();

        colTextGuesses.text = Global.STRING_OVERVIEW_GUESSES;
        colTextCorrectGuesses.text = Global.STRING_OVERVIEW_CORRECT_GUESSES;
        colTextBonusTime.text = Global.STRING_OVERVIEW_BONUS_TIME;
        colTextLevel.text = Global.STRING_OVERVIEW_LEVEL;
    }
}
