﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using Utils;

public class UINumberGuessing : UIElement
{
    //Header
    [SerializeField] private GameObject headerHolder;
    private UIGameHeaderWithSlider gameHeaderWithSliderUI;

    //Hint Area
    [SerializeField] private Text fromText;
    [SerializeField] private Text toText;
    [SerializeField] private InputField inputField;
    
    //Guess Area
    [SerializeField] private GameObject guessArea;
    [SerializeField] private Button guessButton;
    private UISimpleListGroup tooLowList;
    private UISimpleListGroup tooHighList;

    //Bottom
    [SerializeField] private GameObject numPadHolder;
    private UINumPad numPadUI;

    void Awake()
    {
        //init header
        gameHeaderWithSliderUI = UIElement.Instantiate<UIGameHeaderWithSlider>(headerHolder.transform);

        //setup left guess list
        tooLowList = UIElement.Instantiate<UISimpleListGroup>(guessArea.transform);
        tooLowList.SetAlignmentOfScrollbar(false);
        tooLowList.SetListTitle(Global.STRING_IT_NUMBER_TOO_LOW);

        //setup right guess list
        tooHighList = UIElement.Instantiate<UISimpleListGroup>(guessArea.transform);
        tooHighList.SetAlignmentOfScrollbar(true);
        tooHighList.SetListTitle(Global.STRING_IT_NUMBER_TOO_HIGH);

        //setup numpad
        numPadUI = UIElement.Instantiate<UINumPad>(numPadHolder.transform);
        numPadUI.SetInputField(inputField);

        //arrange lists: low -> left, high -> right
        tooLowList.transform.SetAsFirstSibling();
        tooHighList.transform.SetAsLastSibling();
    }

    void Update()
    {
        //enable/disable guess button
        guessButton.interactable = inputField.text.Length > 0;
    }

    public UINumPad GetNumPadUI()
    {
        return numPadUI;
    }

    public void SetGameSlider(UICustomSlider slider)
    {
        gameHeaderWithSliderUI.SetSlider(slider);
    }

    public string GetInputText()
    {
        return inputField.text;
    }

    public void AddGuessButtonAction(UnityAction guessButtonAction)
    {
        guessButton.onClick.AddListener(guessButtonAction);
        numPadUI.AddOnSubmitAction(guessButtonAction);
    }

    public void AddPauseButtonAction(UnityAction pauseButtonAction)
    {
        gameHeaderWithSliderUI.AddOnPauseAction(pauseButtonAction);
    }
    public void SetPauseButtonImage(Sprite image)
    {
        gameHeaderWithSliderUI.SetPauseButtonImage(image);
    }
    public void RemovePauseButton()
    {
        gameHeaderWithSliderUI.RemovePauseButton();
    }

    public void ClearInputText()
    {
        inputField.text = "";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="guessedValue">value that has been guessed and shall be displayed</param>
    /// <param name="attemptNumber">number which indicates how many numbers the user already guessed</param>
    /// <param name="tooHigh">bool which determines if the guess item will be added to the "too high list" or to the "too low list"</param>
    public UIGuessItem AddGuessItem(int attemptNumber, string guessedValue, bool tooHigh)
    {
        UIGuessItem guessItem = CreateGuessItem(attemptNumber, guessedValue);

        UISimpleListGroup listGroup = tooHigh ? tooHighList : tooLowList;
        PutGuessItem(listGroup, guessItem);

        return guessItem;
    }

    private UIGuessItem CreateGuessItem(int attemptNumber, string guessedValue)
    {
        UIGuessItem guessItem = UIElement.Instantiate<UIGuessItem>();
        guessItem.SetAttemptNumber(attemptNumber);
        guessItem.SetGuessedValue(guessedValue);

        return guessItem;
    }

    private void PutGuessItem(UISimpleListGroup listGroup, UIGuessItem guessItem)
    {
        listGroup.AddItem(guessItem.gameObject);
        guessItem.transform.SetAsFirstSibling(); //put at top of the stack
    }

    public void SetTitleText(string text)
    {
        gameHeaderWithSliderUI.SetTitleText(text);
    }

    public void SetRoundInfo(ITNumberGuessingRound round, string levelCountText)
    {
        fromText.text = round.GetRangeStart().ToString();
        toText.text = round.GetRangeEnd().ToString();

        gameHeaderWithSliderUI.SetLevelCountText(levelCountText);

        //clear guesses from previous round
        tooHighList.Clear();
        tooLowList.Clear();
    }
}
