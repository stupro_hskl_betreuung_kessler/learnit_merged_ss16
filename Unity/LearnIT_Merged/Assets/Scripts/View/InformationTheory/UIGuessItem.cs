﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Utils;
using System.Linq;

public class UIGuessItem : UIElement
{

    [SerializeField] private Text attemptNumberText;

    [SerializeField] private Text guessedValueText;

    [SerializeField] private Image attemptNumberBackground;

    public void SetAttemptNumber(int attemptNumber)
    {
        attemptNumberText.text = attemptNumber.ToString()+".";
    }
    public void SetGuessedValue(string guessedValue)
    {
        guessedValueText.text = guessedValue;
    }
    public void SetAttemptNumberBackgroundColor(Color color)
    {
        attemptNumberBackground.color = color;
    }
}
