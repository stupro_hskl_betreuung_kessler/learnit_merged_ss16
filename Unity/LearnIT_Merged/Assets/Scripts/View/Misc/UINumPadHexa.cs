﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;
using System;

public class UINumPadHexa : UINumPad
{
    
   
    public override void Update()
    {
        if (!allowKeyboardInput)
            return;

        KeyCode pressedKey = InputUtils.GetPressedKey();
        switch (pressedKey)
        {
            case KeyCode.Keypad0:
            case KeyCode.Alpha0:
                AppendDigit("0");
                break;
            case KeyCode.Keypad1:
            case KeyCode.Alpha1:
                AppendDigit("1");
                break;
            case KeyCode.Keypad2:
            case KeyCode.Alpha2:
                AppendDigit("2");
                break;
            case KeyCode.Keypad3:
            case KeyCode.Alpha3:
                AppendDigit("3");
                break;
            case KeyCode.Keypad4:
            case KeyCode.Alpha4:
                AppendDigit("4");
                break;
            case KeyCode.Keypad5:
            case KeyCode.Alpha5:
                AppendDigit("5");
                break;
            case KeyCode.Keypad6:
            case KeyCode.Alpha6:
                AppendDigit("6");
                break;
            case KeyCode.Keypad7:
            case KeyCode.Alpha7:
                AppendDigit("7");
                break;
            case KeyCode.Keypad8:
            case KeyCode.Alpha8:
                AppendDigit("8");
                break;
            case KeyCode.Keypad9:
            case KeyCode.Alpha9:
                AppendDigit("9");
                break;
			case KeyCode.A:
				AppendDigit ("a");
				break;
			case KeyCode.B:
				AppendDigit ("b");
				break;
			case KeyCode.C:
				AppendDigit ("c");
				break;
			case KeyCode.D:
				AppendDigit ("d");
				break;
			case KeyCode.E:
				AppendDigit ("e");
				break;
			case KeyCode.F:
				AppendDigit ("f");
				break;
            case KeyCode.Delete:
            case KeyCode.Backspace:
                RemoveDigit();
                break;
            case KeyCode.KeypadEnter:
            case KeyCode.Return:
                OnSubmit();
                break;

            default: break;
        }
    }
}
