﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIProgressSlider : UICustomSlider {

    protected override void Start()
    {
        Reset();
    }

    void Update()
    {
        if (finished)
            return;

        //check wether progress complete
        if (currentValue >= maxValue)
        {
            finished = true;
            currentValue = maxValue;
            if (null != onFinishedHandler)
                NotifyAboutSliderFinished();
            else
                Debug.Log("ProgressSlider finished. No OnFinishedListeners set.");
        }
    }

    protected override void UpdateText()
    {
        int percent = Mathf.RoundToInt(GetValueRatio() * 100);
        valueText.text = string.Format("{0}{1}{2}", prependingText, percent, appendingText);
    }

    public override void Reset()
    {
        finished = false;
        maxValue = 100;
        currentValue = 0;
        UpdateSlider();
    }

    /// <summary>
    /// Resets the slider and sets its progress.
    /// </summary>
    /// <param name="progress">Value should be between 0 and 1, where 0 represents 0% progress and 1 means 100% progress</param>
    public override void Reset(float progress)
    {
        Reset();
        UpdateProgress(progress);
    }

    /// <summary>
    /// Sets the progress of the slider.
    /// </summary>
    /// <param name="progress">Value should be between 0 and 1, where 0 represents 0% progress and 1 means 100% progress</param>
    public void UpdateProgress(float progress)
    {
        if (progress < 0)
            progress = 0;

        else if (progress > 1)
            progress = 1;

        currentValue = progress*100;
        UpdateSlider();
    }
}
