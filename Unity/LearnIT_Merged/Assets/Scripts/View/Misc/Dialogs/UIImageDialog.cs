﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;

public class UIImageDialog : UIDialog
{
    [SerializeField] private Image image;
    [SerializeField] protected Button btnDismiss;

    protected override void Awake()
    {
        base.Awake();
        AddOnCloseAction(Hide);
    }

    void Update()
    {
        if (!allowKeyboardInput || !IsShowing())
            return;

        KeyCode pressedKey = InputUtils.GetPressedKey();
        switch (pressedKey)
        {
            case KeyCode.Return:
            case KeyCode.KeypadEnter:
            case KeyCode.Escape:
                btnDismiss.onClick.Invoke();
                break;
        }
    }

    public override void AddOnCloseAction(UnityEngine.Events.UnityAction onCloseCallback)
    {
        base.AddOnCloseAction(onCloseCallback);

        if (null != onCloseCallback)
            btnDismiss.onClick.AddListener(onCloseCallback);
    }

    public override void ClearOnCloseActions()
    {
        base.ClearOnCloseActions();
        btnDismiss.onClick.RemoveAllListeners();
    }

    public void Setup(string dialogTitle, Sprite image, UnityAction onCloseCallback = null)
    {
        SetDialogTitle(dialogTitle);
        SetImage(image);

        ClearOnCloseActions();
        AddOnCloseAction(onCloseCallback);
        AddOnCloseAction(Hide);
    }

    public void SetImage(Sprite image)
    {
        this.image.sprite = image;
    }
}
