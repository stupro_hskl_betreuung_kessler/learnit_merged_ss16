﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;
using UnityEngine.EventSystems;

public abstract class UIDialog : UIElement
{
    [SerializeField] private Canvas canvas;
    [SerializeField] protected Image overlayImage;
    [SerializeField] protected GameObject contentHolder;

    [SerializeField] protected GameObject headerHolder;
    protected UIDialogHeader header;

    [SerializeField] protected bool allowKeyboardInput = true;

    public void SetDialogTitle(string title)
    {
        header.SetTitleText(title);
    }

    public bool IsShowing()
    {
        return gameObject.activeSelf;
    }

    public virtual void AddOnCloseAction(UnityAction onCloseCallback)
    {
        if (null != onCloseCallback)
            header.GetCloseButton().onClick.AddListener(onCloseCallback);
    }

    public virtual void ClearOnCloseActions()
    {
        header.GetCloseButton().onClick.RemoveAllListeners();
    }

    public GameObject GetContentHolder()
    {
        return contentHolder;
    }

    protected virtual void Awake()
    {
        header = UIElement.Instantiate<UIDialogHeader>(headerHolder.transform);
        Hide();

        //make draggable at title
        GameObject titleHolder = header.GetTitle().gameObject;
        DragHandler titleDragger = titleHolder.AddComponent<DragHandler>(); //make dialog draggable
        titleDragger.SetTransformToDrag(contentHolder.transform); //move whole content, when title is being dragged
        
    }

    protected void ExecuteOnCloseActions()
    {
        header.GetCloseButton().onClick.Invoke();
    }
}
