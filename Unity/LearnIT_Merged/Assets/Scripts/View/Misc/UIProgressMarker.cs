﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using Utils;

public class UIProgressMarker : UIElement
{
    [SerializeField] private Text progressNumber;
    [SerializeField] private Image progressCorrect;
    [SerializeField] private Image progressWrong;
    [SerializeField] private Image progressBackground;

    [SerializeField] Color numberBGColor;
    [SerializeField] Color correctBGColor = Global.COLOR_FLASH_GREEN;
    [SerializeField] Color wrongBGColor = Global.COLOR_FLASH_RED;

    public void SetNumber(int num)
    {
        progressNumber.text = num.ToString();
        progressNumber.gameObject.SetActive(true);
        progressCorrect.gameObject.SetActive(false);
        progressWrong.gameObject.SetActive(false);
        progressBackground.color = numberBGColor;
    }

    public void SetCorrect()
    {
        progressNumber.gameObject.SetActive(false);
        progressCorrect.gameObject.SetActive(true);
        progressWrong.gameObject.SetActive(false);
        progressBackground.color = correctBGColor;
    }

    public void SetWrong()
    {
        progressNumber.gameObject.SetActive(false);
        progressCorrect.gameObject.SetActive(false);
        progressWrong.gameObject.SetActive(true);
        progressBackground.color = wrongBGColor;
    }
}
