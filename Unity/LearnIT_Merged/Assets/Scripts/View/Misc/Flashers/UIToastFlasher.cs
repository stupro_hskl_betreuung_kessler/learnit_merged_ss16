﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof (CanvasGroup))]
public class UIToastFlasher : UICanvasGroupFlasher {

    public static void CreateAndDisplayToast(MonoBehaviour coRoutineServer, Color textColor, Color bgColor, string toastMessage)
    {
        if (null == coRoutineServer)
            return;

        coRoutineServer.StartCoroutine(CreateAndDisplayToastRoutine(coRoutineServer, textColor, bgColor, toastMessage));
    }

    private static IEnumerator CreateAndDisplayToastRoutine(MonoBehaviour coRoutineServer, Color textColor, Color bgColor, string toastMessage)
    {
        UIToastFlasher toaster = UIElement.Instantiate<UIToastFlasher>();
        DontDestroyOnLoad(toaster); //also show message while scene is being changed
        yield return coRoutineServer.StartCoroutine(toaster.Flash(textColor, bgColor, toastMessage));
        Object.Destroy(toaster.gameObject); //destroy message after being shown
    }

    public Text textOfCanvasGroupToBeFlashed;
    public Image imageOfCanvasGroupToBeFlashed;

    public IEnumerator Flash(Color colorToFlashTextIn, Color colorToFlashImageIn)
    {
        Color tmp1 = textOfCanvasGroupToBeFlashed.color;
        Color tmp2 = imageOfCanvasGroupToBeFlashed.color;
        textOfCanvasGroupToBeFlashed.color = colorToFlashTextIn;
        imageOfCanvasGroupToBeFlashed.color = colorToFlashImageIn;
        yield return StartCoroutine(base.Flash());
        textOfCanvasGroupToBeFlashed.color = tmp1;
        imageOfCanvasGroupToBeFlashed.color = tmp2;
    }

    public IEnumerator Flash(Color colorToFlashTextIn, Color colorToFlashImageIn, string textToFlash)
    {
        string tmp = textOfCanvasGroupToBeFlashed.text;
        textOfCanvasGroupToBeFlashed.text = textToFlash;
        yield return StartCoroutine(Flash(colorToFlashTextIn, colorToFlashImageIn));
        textOfCanvasGroupToBeFlashed.text = tmp;
    }

    public IEnumerator Flash(string textToFlash)
    {
        string tmp = textOfCanvasGroupToBeFlashed.text;
        textOfCanvasGroupToBeFlashed.text = textToFlash;
        yield return StartCoroutine(base.Flash());
        textOfCanvasGroupToBeFlashed.text = tmp;
    }

}
