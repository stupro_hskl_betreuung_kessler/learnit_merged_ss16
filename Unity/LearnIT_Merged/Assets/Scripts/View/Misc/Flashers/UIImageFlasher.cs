﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof (CanvasGroup))]
public class UIImageFlasher : UICanvasGroupFlasher {

    public Image imageOfCanvasGroupToBeFlashed;

    public IEnumerator Flash(Color colorToFlashImageIn)
    {
        Color tmp = imageOfCanvasGroupToBeFlashed.color;
        imageOfCanvasGroupToBeFlashed.color = colorToFlashImageIn;
        yield return StartCoroutine(base.Flash());
        imageOfCanvasGroupToBeFlashed.color = tmp;
    }

}
