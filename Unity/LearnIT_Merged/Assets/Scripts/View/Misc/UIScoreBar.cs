﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIScoreBar : UICustomSlider
{


    [SerializeField]
    protected Text playerLocalName;
    [SerializeField]
    protected Text playerLocalScore;
    [SerializeField]
    protected Text playerEnemyName;
    [SerializeField]
	protected Text playerEnemyScore;
    [SerializeField]
	protected Text colon;
    [SerializeField]
	protected Image playerEnemyColor;
    [SerializeField]
	protected Image playerLocalColor;

	protected int scoreLocal;
	protected int scoreEnemy;

	public virtual int ScoreEnemy
    {
        get
        {
            return scoreEnemy;
        }
        set
        {
            scoreEnemy = value;
            playerEnemyScore.text = "" + value;
        }
    }

	public virtual int ScoreLocal
    {
        get
        {
            return scoreLocal;
        }
        set
        {
            scoreLocal = value;
            playerLocalScore.text = "" + value;
        }
    }

    public string LocalName
    {
        get
        {
            return playerLocalName.text;
        }
        set
        {
            playerLocalName.text = value;
        }
    }

    public string EnemyName
    {
        get
        {
            return playerEnemyName.text;
        }
        set
        {
            playerEnemyName.text = value;
        }
    }

    public Color PlayerLocalColor
    {
        get
        {
            return playerLocalColor.color;
        }
        set
        {
            playerLocalColor.color = value;
        }
    }

    public Color PlayerEnemyColor
    {
        get
        {
            return playerEnemyColor.color;
        }
        set
        {
            playerEnemyColor.color = value;
        }
    }

    public void SetScoreTextColor(Color color)
    {
        playerLocalScore.color = color;
        playerEnemyScore.color = color;
        colon.color = color;
    }
}