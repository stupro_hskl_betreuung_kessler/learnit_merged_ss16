﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.IO;


public class UIVideoDialog : UIDialog
{
    [SerializeField] private GameObject videoHolder;

    public void PlayVideo(string videotitle, string filename, UnityAction onFinishCallback=null)
    {
        Show();

        header.SetTitleText(videotitle);


#if UNITY_STANDALONE
        //play video on standalone platform. loading ogv from streaming assets
        StartCoroutine(PlayMovieStandalone(filename + ".ogv", onFinishCallback)); //
        //PlayMovieResources("Video/test"); //loading from resources (no extension)

#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
        //play video on mobile devices. loading mp4 from streaming assets
        StartCoroutine(PlayMovieHandheld(filename+".mp4", onFinishCallback));
        
#else
        //return without playing
        Debug.LogError("Can not play video. Platform not supported");
#endif
    }

#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
    private IEnumerator PlayMovieHandheld(string videoPath, UnityAction onFinishCallback)
    {
        //Debug.Log("playing handheld " + videoPath);
        Handheld.PlayFullScreenMovie(videoPath, Color.black, FullScreenMovieControlMode.Hidden);

        //return to previous scene when returning from video player. 
        //we can simply wait here, since PlayFullScreen pauses unity and returns when finished.
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        //TODO: maybe give feedback of video end, ask for replay?
        Hide();

        if(null!=onFinishCallback)
            onFinishCallback.Invoke();
    }
#endif

#if UNITY_STANDALONE

    /*
     * Stream attempt - only works for .ogv, which is not supported by mobile
     */
    private IEnumerator PlayMovieStandalone(string videoPath, UnityAction onFinishCallback)
    {
        videoPath = "file:///" + Application.streamingAssetsPath + Path.AltDirectorySeparatorChar + videoPath;
        //Debug.Log("playing standalone: " + videoPath);

        //stream video (file can be exchanged after build) http://docs.unity3d.com/ScriptReference/Application-streamingAssetsPath.html
        WWW www = new WWW(videoPath);
        yield return www;
        
        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.LogError(www.error);
            UIToastFlasher.CreateAndDisplayToast(this, Color.white, Global.COLOR_FLASH_RED, www.error);
            Hide();
            if (null != onFinishCallback)
                onFinishCallback.Invoke();

            yield break;
        }

        // Make sure the movie is ready to start before we start playing
        MovieTexture mt = www.movie;
        while (!mt.isReadyToPlay){
            yield return null;
        }

        //add movietexture and audiosource (see http://docs.unity3d.com/ScriptReference/MovieTexture-audioClip.html)
        RawImage ri = videoHolder.AddComponent<RawImage>();
        ri.texture = mt;

        AudioSource aud = videoHolder.AddComponent<AudioSource>();
        aud.playOnAwake = false;
        aud.clip = mt.audioClip;

        //play audio and video
        Show();
        mt.Play();
        aud.Play();

        UnityAction videoEndHandler = () =>
        {
            Destroy(mt);
            Destroy(ri);
            Destroy(aud);
            Hide();

            if (null != onFinishCallback)
                onFinishCallback.Invoke();
        };

        ClearOnCloseActions();
        AddOnCloseAction(videoEndHandler);

        //TODO: maybe give feedback of video end, ask for replay. video controls?
        StartCoroutine(WaitForVideoEnd(mt, videoEndHandler));  
    }

    /*
    * Resources attempt - only works when video is imported in resources folder, but this prevents deployment for android
    private void PlayMovieResources(string videoPath)
    {
        MovieTexture mt = Resources.Load<MovieTexture>(videoPath);
        RawImage ri = gameObject.AddComponent<RawImage>();
        ri.texture = mt;
        mt.Play();

        StartCoroutine(
            OnVideoEnd(mt, sceneLoader.LoadPreviousScene)
        );  
    }*/

    private IEnumerator WaitForVideoEnd(MovieTexture mt, UnityAction callback)
    {
        while (mt!=null && mt.isPlaying)
        {
            yield return 0;
        }

        callback();
        yield break;
    }


#endif
}
	
