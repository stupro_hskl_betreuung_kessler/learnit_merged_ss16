﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIScoreBarRope:UIScoreBar
{

	[SerializeField] private Image imageLocal, imageEnemy;
	private TOWSmoothScrollingHelper scrollingHelper = new TOWSmoothScrollingHelper(3F);


	private void adjustSlider()
	{
		scrollingHelper.updateTargetScaling (scoreLocal, scoreEnemy);
		imageLocal.transform.localScale = new Vector3 (scrollingHelper.getCurrentScale (), 1);
	}

	public void UpdateScrollBar()
	{
		if(!scrollingHelper.finishedMoving ())
		{
			imageLocal.transform.localScale = new Vector3 (scrollingHelper.getCurrentScale (), 1);
		}
	}

	public override int ScoreEnemy{
		get{
			return scoreEnemy;
		}
		set{
			scoreEnemy = value;
			playerEnemyScore.text = "" + value;
			adjustSlider ();
		}
	}

	public override int ScoreLocal{
		get{
			return scoreLocal;
		}
		set{
			scoreLocal = value;
			playerLocalScore.text = "" + value;
			adjustSlider ();
		}
	}
}