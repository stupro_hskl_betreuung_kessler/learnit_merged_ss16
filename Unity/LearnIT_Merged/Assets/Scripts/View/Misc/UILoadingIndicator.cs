﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using Utils;

public class UILoadingIndicator : UIElement
{
    [SerializeField] private Text indicationText;
    [SerializeField] private Texture loadingGraphic;
    [SerializeField] private float size = 100.0f;
    [SerializeField] private float rotationAngle = 0.0f;
    [SerializeField] private float rotationSpeed = 300.0f;
    [SerializeField] private bool isCancelAble;
    [SerializeField] private Button cancelButton;
    [SerializeField] private bool allowKeyboardInput;
    [SerializeField] private GameObject progressBarHolder;
    [SerializeField] private bool showProgressBar;
    private UIProgressSlider progressBar;

    private bool isLoading = false;

    void Awake()
    {
        progressBar = UIElement.Instantiate<UIProgressSlider>(progressBarHolder.transform);
        progressBar.Reset();
    }

    void Start()
    {
        if(isCancelAble)
            AddOnCancelAction(Hide);
    }

    void Update()
    {
        if (!isLoading)
            return;

        //update angle value, which will be applied in OnGUI
        rotationAngle += rotationSpeed * Time.deltaTime;


        if (!allowKeyboardInput)
            return;

        //cancel on esc press
        KeyCode pressedKey = InputUtils.GetPressedKey();
        switch (pressedKey)
        {
            case KeyCode.Escape:
                cancelButton.onClick.Invoke();
                break;
        }
	}

    void OnGUI() {
        if (!isLoading)
            return;

        //spin around center
        Vector2 pivot = new Vector2(Screen.width/2, Screen.height/2);
        GUIUtility.RotateAroundPivot(rotationAngle % 360, pivot);
        GUI.DrawTexture(new Rect((Screen.width - size) / 2, (Screen.height - size) / 2, size, size), loadingGraphic); 
    }

    public void SetIndicationText(string text)
    {
        if(null!=indicationText)
            indicationText.text = text;
    }

    public void SetIsCancelAble(bool isCancelAble)
    {
        this.isCancelAble = isCancelAble;

        if(isLoading && null!=cancelButton)
            cancelButton.gameObject.SetActive(isCancelAble);

    }

    public void SetShowProgressBar(bool showProgressBar)
    {
        this.showProgressBar = showProgressBar;

        if (isLoading)
            progressBarHolder.gameObject.SetActive(showProgressBar);
    }

    public override void Show()
    {
        base.Show();
        isLoading = true;

        if (null != cancelButton)
            cancelButton.gameObject.SetActive(isCancelAble);

        progressBarHolder.gameObject.SetActive(showProgressBar);
    }

    public override void Hide()
    {
        base.Hide();
        isLoading = false;

        if (null != cancelButton)
            cancelButton.gameObject.SetActive(false);
    }

    public void AddOnCancelAction(UnityAction onCancelAction)
    {
        if (null == onCancelAction || null == cancelButton)
            return;

        SetIsCancelAble(true);
        cancelButton.onClick.AddListener(onCancelAction);
    }

    public void ClearOnCancelActions()
    {
        if (null == cancelButton)
            return;

        cancelButton.onClick.RemoveAllListeners();
        AddOnCancelAction(Hide);
    }

    /// <summary>
    /// Updates the value of the progress bar
    /// </summary>
    /// <param name="progress">value between 0 and 1 which indicates the progress in percent</param>
    public void UpdateProgress(float progress)
    {
        if(null==progressBar)
            return;

        progressBar.UpdateProgress(progress);
    }

    public void SetPrependingProgressBarText(string text)
    {
        if (null != text)
            progressBar.SetPrependingText(text);
    }

    public void SetAppendingProgressBarText(string text)
    {
        if (null != text)
            progressBar.SetAppendingText(text);
    }
}
