﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;

public class UISettingsMenu : UIDialog
{
    [SerializeField] private GameObject sliderHolder;
    [SerializeField] private GameObject sliderGroupPrefab;
    [SerializeField] private Button quitButton;
    [SerializeField] private Button okButton;
    [SerializeField] private Button resetButton;
    [SerializeField] private Toggle isMutedToggle;
    [SerializeField] private Text isMutedText;

    private UISimpleSliderGroup musicVolumeSliderGroup;
    private UISimpleSliderGroup sfxVolumeSliderGroup;

    public UISimpleSliderGroup GetMusicVolumeSliderGroup()
    {
        return musicVolumeSliderGroup;
    }

    public UISimpleSliderGroup GetSFXVolumeSliderGroup()
    {
        return sfxVolumeSliderGroup;
    }

    public void SetMusicVolume(float volume)
    {
        musicVolumeSliderGroup.GetSlider().value = volume;
    }

    public float GetMusicVolume()
    {
        return musicVolumeSliderGroup.GetSlider().value;
    }

    public void SetSfxVolume(float volume)
    {
        sfxVolumeSliderGroup.GetSlider().value = volume;
    }

    public float GetSFXVolume()
    {
        return sfxVolumeSliderGroup.GetSlider().value;
    }

    public void AddOnMusicVolumeChangedListener(UnityAction<float> onMusicVolumeChangedListener)
    {
        musicVolumeSliderGroup.AddOnValueChangedListener(onMusicVolumeChangedListener);
    }

    public void SetOnMusicVolumeEndDragListener(UnityAction<float> endDragListener)
    {
        musicVolumeSliderGroup.SetOnEndDragHandler(endDragListener);
    }

    public void AddOnSfxVolumeChangedListener(UnityAction<float> onSfxVolumeChangedListener)
    {
        sfxVolumeSliderGroup.AddOnValueChangedListener(onSfxVolumeChangedListener);
    }

    public void SetOnSfxVolumeEndDragListener(UnityAction<float> endDragListener)
    {
        sfxVolumeSliderGroup.SetOnEndDragHandler(endDragListener);
    }

    public bool MusicSliderIsBeingDragged()
    {
        return musicVolumeSliderGroup.IsBeingDragged();
    }

    public bool SfxSliderIsBeingDragged()
    {
        return sfxVolumeSliderGroup.IsBeingDragged();
    }

    public bool IsMuted()
    {
        return isMutedToggle.isOn;
    }

    public void SetIsMuted(bool isMuted)
    {
        isMutedToggle.isOn = isMuted;
    }

    protected override void Awake()
    {
        base.Awake();
        musicVolumeSliderGroup = UIElement.Instantiate<UISimpleSliderGroup>(sliderHolder.transform);
        sfxVolumeSliderGroup = UIElement.Instantiate<UISimpleSliderGroup>(sliderHolder.transform);

        header.SetTitleText(Global.STRING_SETTINGS_TITLE);
        musicVolumeSliderGroup.SetDescriptionText(Global.STRING_SETTINGS_MUSICVOL);
        sfxVolumeSliderGroup.SetDescriptionText(Global.STRING_SETTINGS_SFXVOL);
        isMutedText.text = Global.STRING_SETTINGS_MUTE;

        SceneLoader sceneLoader = gameObject.AddComponent<SceneLoader>();
        okButton.onClick.AddListener(ExecuteOnCloseActions);
        quitButton.onClick.AddListener(ExecuteOnCloseActions);
        quitButton.onClick.AddListener(sceneLoader.QuitApplication);

        //todo: remove reset button
        resetButton.onClick.AddListener(PlayerPrefs.DeleteAll);
    }

    public void SetOnMuteHandler(UnityAction<bool> onMuteHandler)
    {
        if (null == onMuteHandler)
            return;

        isMutedToggle.onValueChanged.RemoveAllListeners();
        isMutedToggle.onValueChanged.AddListener(onMuteHandler);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ExecuteOnCloseActions();
        }
    }
}
  
