﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIGameMenuModeInfo : UIElement
{
    [SerializeField] private Text txtHeader;
    [SerializeField] private Text txtModeTitle;
    [SerializeField] private Text txtModeDescription;
    [SerializeField] private Image imgModeImage;
    [SerializeField] private Button btnPlay;
    [SerializeField] private Button btnTutorial;

    void Awake()
    {
        ClearMode();
    }

    /// <summary>
    /// Updates the UI of the ModeInfo according to the given mode
    /// </summary>
    /// <param name="mode"></param>
    public void SetMode(GameMode mode)
    {
        SetModeTitle(mode.GetName());
        SetModeDescription(mode.GetDescription());
        SetModeSprite(mode.GetSpriteImage());

        btnTutorial.gameObject.SetActive(mode.HasTutorial());
    }

    public void ClearMode()
    {
        SetModeTitle("");
        SetModeDescription("");
        SetModeSprite(null);
    }

    public void SetHeaderText(string text)
    {
        txtHeader.text = text;
    }

    public void SetModeTitle(string title)
    {
        txtModeTitle.text = title;
    }

    public void SetModeDescription(string description)
    {
        txtModeDescription.text = description;
    }

    public void SetModeSprite(Sprite img)
    {
        imgModeImage.sprite = img;
    }

    public Button GetPlayButton()
    {
        return btnPlay;
    }

    public Button GetTutorialButton()
    {
        return btnTutorial;
    }
}
