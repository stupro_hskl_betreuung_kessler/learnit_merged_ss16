﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;

public class UINumberSystemsWithHistory : UIElement {

    [SerializeField]
    protected GameObject numpadHolder;
    protected UINumPad numpad;
    [SerializeField]
    private InputField leftInputField;
    [SerializeField]
    protected InputField rightInputField;
    [SerializeField]
    private Button submitButton;
    [SerializeField]
    private GameObject headerHolder;
    private UIGameHeaderWithSlider gameHeaderWithSliderUI;
    [SerializeField]
    private Text numberSystemLeftText;
    [SerializeField]
    private Text numberSystemRightText;
    [SerializeField]
    private Text headerAsked;
    [SerializeField]
    private Text headerAnswered;
    [SerializeField]
    private Text headerCorrect;
    [SerializeField]
    private GameObject answerListHolder;
    protected List<UIAnswersEntry> answers;






    private UITextFlasher textFlasherUI;
    private UIImageFlasher screenFlasher;


    // Use this for initialization
    void Awake()
    {
        gameHeaderWithSliderUI = UIElement.Instantiate<UIGameHeaderWithSlider>(headerHolder.transform);
        answers = new List<UIAnswersEntry>();
        headerAnswered.text = Global.ANSWER_TEXT;
        headerAsked.text = Global.ASKED_TEXT;
        headerCorrect.text = Global.CORRECT_TEXT;

        textFlasherUI = UIElement.Instantiate<UITextFlasher>(this.transform);
        textFlasherUI.transform.SetAsLastSibling();
        screenFlasher = UIElement.Instantiate<UIImageFlasher>(this.transform);
        screenFlasher.transform.SetAsLastSibling();
    }

    // Update is called once per frame
    void Update()
    {
        submitButton.interactable = rightInputField.text.Length > 0;
    }


    public void AddPauseButtonAction(UnityAction pauseButtonAction)
    {
        gameHeaderWithSliderUI.AddOnPauseAction(pauseButtonAction);
    }

    public void SetLeftNumberSystemName(string name)
    {
        this.numberSystemLeftText.text = name;
    }

    public void SetRightNumberSystemName(string name)
    {
        this.numberSystemRightText.text = name;
    }

    public UINumPad GetNumPad()
    {
        return numpad;
    }

    public virtual void CreateNumPad(NumberCreator creator)
    {
        if (creator.getRightSystem() > 10)
        {
            numpad = UIElement.Instantiate<UINumPadHexa>(numpadHolder.transform);
        }
        else
        {
            numpad = UIElement.Instantiate<UINumPad>(numpadHolder.transform);
        }
        numpad.SetInputField(rightInputField);
    }

    public Button GetSubmitButton()
    {
        return submitButton;
    }

    public InputField GetInputFieldLeft()
    {
        return leftInputField;
    }

    public InputField GetInputFieldRight()
    {
        return rightInputField;
    }

    public UIImageFlasher GetScreenFlasher()
    {
        return screenFlasher;
    }

    public UITextFlasher GetTextFlasher()
    {
        return textFlasherUI;
    }

    public void SetGameSlider(UICustomSlider slider)
    {
        gameHeaderWithSliderUI.SetSlider(slider);
    }

    public void SetTitleText(string s)
    {
        gameHeaderWithSliderUI.SetTitleText(s);
    }

    public void SetGameLevel(int currentLevel, int maxLevel)
    {
        gameHeaderWithSliderUI.SetLevelText(Global.LEVEL_TEXT);
        gameHeaderWithSliderUI.SetLevelCountText(currentLevel + " / " + maxLevel);
    }

    public void SetGameLevel(int currentLevel)
    {
        gameHeaderWithSliderUI.SetLevelText(Global.LEVEL_TEXT);
        gameHeaderWithSliderUI.SetLevelCountText("" + currentLevel);
    }

    public void AddAnswer(string askedNumber, string answeredNumber, string correctNumber)
    {
        UIAnswersEntry answer = UIElement.Instantiate<UIAnswersEntry>(answerListHolder.transform);
        answer.transform.SetAsFirstSibling();
        answer.SetEntries(askedNumber, answeredNumber, correctNumber);
        answers.Add(answer);
    }

    public List<UIAnswersEntry> GetAnswers()
    {
        return answers;
    }
}
