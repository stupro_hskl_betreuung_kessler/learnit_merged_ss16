﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MPUINumberSystemsCoopOverview : UIGameOverview {

    [SerializeField]
    private Text resultText;
    [SerializeField]
    private Image resultImage;

    public Text ResultText
    {
        get
        {
            return resultText;
        }
    }

    public Image ResultImage
    {
        get
        {
            return resultImage;
        }
        set
        {
            resultImage = value;
        }
    }
}
