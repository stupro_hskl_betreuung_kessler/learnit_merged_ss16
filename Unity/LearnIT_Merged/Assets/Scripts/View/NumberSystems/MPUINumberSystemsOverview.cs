﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MPUINumberSystemsOverview : UIGameOverview
{
    [SerializeField] private Text winnerText;
    [SerializeField] private GameObject sliderHolder;

	private UICustomSlider slider;

	public Text WinnerText
	{
		get
		{
			return winnerText;
		}
	}

	public UICustomSlider Slider
	{
		get
		{
			return slider;
		}
		set
		{
			slider = value;
		}
	}

	public GameObject SliderHolder
	{
		get
		{
			return sliderHolder;
		}
	}
}