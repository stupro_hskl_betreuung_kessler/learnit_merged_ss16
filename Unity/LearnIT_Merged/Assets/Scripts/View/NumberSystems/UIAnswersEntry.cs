﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIAnswersEntry : UIElement {

    [SerializeField]
    private Text askedNumber;
    [SerializeField]
    private Text answeredNumber;
    [SerializeField]
    private Text correctNumber;
    [SerializeField]
    private Image background;

    public void SetEntries(string askedNumber, string answeredNumber, string correctNumber)
    {
        this.askedNumber.text = askedNumber;
        this.answeredNumber.text = answeredNumber;
        this.correctNumber.text = correctNumber;
        if(answeredNumber == correctNumber)
        {
            background.color = Color.green;
        }
        else
        {
            background.color = Color.red;
        }
    }

    public Text AskedNumber
    {
        get
        {
            return askedNumber;
        }

        set
        {
            askedNumber = value;
        }
    }

    public Text AnsweredNumber
    {
        get
        {
            return answeredNumber;
        }

        set
        {
            answeredNumber = value;
        }
    }

    public Text CorrectNumber
    {
        get
        {
            return correctNumber;
        }

        set
        {
            correctNumber = value;
        }
    }

    public Color BackgroundColor
    {
        get
        {
            return background.color;
        }
        set
        {
            background.color = value;
        }
    }
}
